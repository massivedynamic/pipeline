package massivedynamic.pipeline.core.io;

/**
 * @author lmedina
 */
public interface Reader<T> {
    T read();
}
