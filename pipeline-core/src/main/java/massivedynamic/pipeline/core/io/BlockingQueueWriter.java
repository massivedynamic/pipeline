package massivedynamic.pipeline.core.io;

import org.apache.commons.lang.Validate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.BlockingQueue;

/**
 * @author lmedina
 */
public class BlockingQueueWriter<T> implements Writer<T> {
    private static final Logger LOGGER = LoggerFactory.getLogger(BlockingQueueWriter.class);

    private BlockingQueue<T> queue;

    public BlockingQueueWriter(BlockingQueue<T> queue) {
        Validate.notNull(queue, "queue must not be null.");

        this.queue = queue;
    }

    @Override
    public void write(T value) {
        try {
            if (!queue.offer(value)) {
                LOGGER.warn("Failed to write. Insufficient queue space available.");
            }
        } catch (ClassCastException e) {
            LOGGER.warn("Failed to write. Incompatible classes.", e);
        } catch (IllegalArgumentException e) {
            LOGGER.warn("Failed to write. An unknown property of the value prevented it from being written");
        } catch (NullPointerException e) {
            LOGGER.warn("Failed to write. Value was null.", e);
        }
    }

    // For testing.
    void setQueue(BlockingQueue<T> queue) {
        this.queue = queue;
    }
}
