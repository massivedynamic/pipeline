package massivedynamic.pipeline.core.io;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.function.Supplier;

/**
 * @author lmedina
 */
public class BlockingQueueSupplier<T> implements Supplier<T> {
    private static final Logger LOGGER = LoggerFactory.getLogger(BlockingQueueSupplier.class);

    private BlockingQueue<T> blockingQueue;
    private long timeoutMillis;

    public BlockingQueueSupplier(BlockingQueue<T> blockingQueue, long timeoutMillis) {
        this.blockingQueue = blockingQueue;
        this.timeoutMillis = timeoutMillis;
    }

    @Override
    public T get() {
        try {
            return blockingQueue.poll(timeoutMillis, TimeUnit.MILLISECONDS);
        } catch (InterruptedException e) {
            LOGGER.warn("Read operation was interrupted.", e);
            return null;
        }
    }
}
