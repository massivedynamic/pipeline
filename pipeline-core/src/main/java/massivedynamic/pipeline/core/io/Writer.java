package massivedynamic.pipeline.core.io;

/**
 * @author lmedina
 */
public interface Writer<T> {
    void write(T value);
}
