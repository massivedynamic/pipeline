package massivedynamic.pipeline.core.io;

import org.apache.commons.lang.Validate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;

/**
 * @author lmedina
 */
public class BlockingQueueReader<T> implements Reader<T> {
    private static final Logger LOGGER = LoggerFactory.getLogger(BlockingQueueReader.class);
    private static final long TIMEOUT = 5;

    private BlockingQueue<T> queue;

    public BlockingQueueReader(BlockingQueue<T> queue) {
        Validate.notNull(queue, "queue must not be null.");

        this.queue = queue;
    }

    @Override
    public T read() {
        try {
            return queue.poll(TIMEOUT, TimeUnit.MILLISECONDS);
        } catch (InterruptedException e) {
            LOGGER.warn("Read operation was interrupted.", e);
            return null;
        }
    }

    // For testing.
    void setQueue(BlockingQueue<T> queue) {
        this.queue = queue;
    }
}
