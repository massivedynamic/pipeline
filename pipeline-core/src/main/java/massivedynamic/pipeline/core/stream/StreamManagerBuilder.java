package massivedynamic.pipeline.core.stream;

import com.typesafe.config.Config;
import massivedynamic.pipeline.core.ioc.AbstractBuilder;
import org.apache.commons.lang.Validate;

import java.io.Serializable;
import java.util.concurrent.BlockingQueue;

/**
 * @author lmedina
 */
public abstract class StreamManagerBuilder<T> implements AbstractBuilder<StreamManager>, Serializable {
    protected Config configuration;
    protected BlockingQueue<T> messageQueue;

    public StreamManagerBuilder<T> setConfiguration(Config configuration) {
        Validate.notNull(configuration, "configuration must not be null.");
        this.configuration = configuration;

        return this;
    }

    public StreamManagerBuilder<T> setMessageQueue(BlockingQueue<T> messageQueue) {
        Validate.notNull(messageQueue, "messageQueue must not be null.");
        this.messageQueue = messageQueue;

        return this;
    }

    @Override
    public abstract StreamManager build();
}
