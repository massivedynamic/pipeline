package massivedynamic.pipeline.core.stream;

/**
 * @author lmedina
 */
public interface StreamManager {
    void connect();
    void reconnect();
    void disconnect();
}
