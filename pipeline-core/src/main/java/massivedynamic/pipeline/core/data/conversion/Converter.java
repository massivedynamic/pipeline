package massivedynamic.pipeline.core.data.conversion;

/**
 * @author lmedina
 */
public interface Converter<TFrom, TTo> {
    TTo convert(TFrom data);
}
