package massivedynamic.pipeline.core.ioc;

/**
 * @author lmedina
 */
public interface AbstractFactory<T> {
    T create();
}
