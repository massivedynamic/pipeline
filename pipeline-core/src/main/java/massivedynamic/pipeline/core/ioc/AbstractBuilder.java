package massivedynamic.pipeline.core.ioc;

/**
 * @author lmedina
 */
public interface AbstractBuilder<T> {
    T build();
}
