package massivedynamic.pipeline.core.io;

import ch.qos.logback.classic.Logger;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.Appender;
import massivedynamic.pipeline.test.logging.LoggingArgumentMatcher;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.slf4j.LoggerFactory;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.*;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.argThat;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;

/**
 * @author lmedina
 */
@RunWith(MockitoJUnitRunner.class)
public class BlockingQueueReaderTest {
    private static final String EXPECTED_VALUE = "Test value";

    @Mock
    private Appender<ILoggingEvent> appender;
    @Mock
    private BlockingQueue<String> queue;

    private BlockingQueueReader<String> reader;

    @Before
    public void setUp() {
        ((Logger) LoggerFactory.getLogger(Logger.ROOT_LOGGER_NAME)).addAppender(appender);

        reader = new BlockingQueueReader<>(queue);
    }

    @Test(expected = IllegalArgumentException.class)
    public void constructorThrowsIllegalArgumentException() {
        new BlockingQueueReader<String>(null);
    }

    @Test
    public void validConstructor() {
        assertNotNull("reader must not be null.", reader);
    }

    @Test
    public void successfulRead() throws Exception {
        doReturn(EXPECTED_VALUE).when(queue).poll(anyLong(), any(TimeUnit.class));
        reader.setQueue(queue);

        String actualValue = reader.read();

        assertEquals("actualValue did not match expected value.", EXPECTED_VALUE, actualValue);
    }

    @Test
    public void unsuccessfulRead() {
        assertNull("actualValue must be null.", reader.read());
    }

    @Test
    public void readThrowsInterruptedException() throws InterruptedException {
        doThrow(InterruptedException.class).when(queue).poll(anyLong(), any(TimeUnit.class));
        reader.setQueue(queue);

        String actualValue = reader.read();

        assertNull("actualValue must be null.", actualValue);
        verify(appender).doAppend(argThat(new LoggingArgumentMatcher("Read operation was interrupted.")));
    }
}
