package massivedynamic.pipeline.core.io;

import ch.qos.logback.classic.Logger;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.Appender;
import massivedynamic.pipeline.test.logging.LoggingArgumentMatcher;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.slf4j.LoggerFactory;

import java.util.concurrent.BlockingQueue;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.*;

/**
 * @author lmedina
 */
@RunWith(MockitoJUnitRunner.class)
public class BlockingQueueWriterTest {
    private static final String EXPECTED_VALUE = "Test value";

    @Mock
    private Appender<ILoggingEvent> appender;
    @Mock
    private BlockingQueue<String> queue;

    private BlockingQueueWriter<String> writer;

    @Before
    public void setUp() {
        ((Logger) LoggerFactory.getLogger(Logger.ROOT_LOGGER_NAME)).addAppender(appender);

        writer = new BlockingQueueWriter<>(queue);
    }

    @Test(expected = IllegalArgumentException.class)
    public void constructorThrowsIllegalArgumentException() {
        new BlockingQueueWriter<String>(null);
    }

    @Test
    public void validConstructor() {
        assertNotNull("writer must not be null.", writer);
    }

    @Test
    public void successfulWrite() {
        doReturn(true).when(queue).offer(anyString());
        writer.setQueue(queue);

        writer.write(EXPECTED_VALUE);

        verify(queue).offer(EXPECTED_VALUE);
    }

    @Test
    public void unsuccessfulWrite() {
        doReturn(false).when(queue).offer(anyString());
        writer.setQueue(queue);

        writer.write(EXPECTED_VALUE);

        verify(queue).offer(EXPECTED_VALUE);
        verify(appender).doAppend(argThat(new LoggingArgumentMatcher("Failed to write. Insufficient queue space available.")));
    }

    @Test
    public void writeThrowsClassCastException() {
        doThrow(ClassCastException.class).when(queue).offer(anyString());
        writer.setQueue(queue);

        writer.write(EXPECTED_VALUE);

        verify(queue).offer(EXPECTED_VALUE);
        verify(appender).doAppend(argThat(new LoggingArgumentMatcher("Failed to write. Incompatible classes.")));
    }

    @Test
    public void writeThrowsIllegalArgumentException() {
        doThrow(IllegalArgumentException.class).when(queue).offer(anyString());
        writer.setQueue(queue);

        writer.write(EXPECTED_VALUE);

        verify(queue).offer(EXPECTED_VALUE);
        verify(appender).doAppend(argThat(new LoggingArgumentMatcher("Failed to write. An unknown property of the value prevented it from being written")));
    }

    @Test
    public void writeThrowsNullPointerException() {
        doThrow(NullPointerException.class).when(queue).offer(anyString());
        writer.setQueue(queue);

        writer.write(EXPECTED_VALUE);

        verify(queue).offer(EXPECTED_VALUE);
        verify(appender).doAppend(argThat(new LoggingArgumentMatcher("Failed to write. Value was null.")));
    }
}
