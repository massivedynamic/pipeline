package massivedynamic.pipeline.node;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.PoisonPill;
import akka.contrib.pattern.ClusterSharding;
import akka.contrib.pattern.ClusterSingletonManager;
import akka.contrib.pattern.ClusterSingletonProxy;
import akka.persistence.Persistent;
import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import massivedynamic.pipeline.actor.feed.Stream;
import massivedynamic.pipeline.actor.feed.StreamActor;
import massivedynamic.pipeline.actor.feed.StreamSupervisor;
import massivedynamic.pipeline.actor.feed.WatchDogActor;
import massivedynamic.pipeline.actor.sharding.MessageExtractor;
import massivedynamic.pipeline.actor.sharding.NonRebalancingLeastShardAllocationStrategy;
import massivedynamic.pipeline.feed.twitter.hbc.TwitterStreamManagerBuilder;

import java.io.File;

/**
 * @author lmedina
 */
public class Node {
    private Config configuration;
    private ActorSystem system;

    private ActorRef watchdog;

    private static Node node = new Node();

    public static void main(String[] args) {
        node.init();
        node.start();
    }

    private void init() {
        configuration = ConfigFactory.parseFile(new File("configuration/application.conf")).withFallback(ConfigFactory.load());//ConfigFactory.parseFile(new File("configuration/akka/application.conf")).withFallback(ConfigFactory.load());
        system = ActorSystem.create("Node", configuration);

        createStreamWatchDog();
        shardStreams();
    }

    private void createStreamWatchDog() {
        system.actorOf(ClusterSingletonManager.defaultProps(WatchDogActor.props(), "WatchDogSingleton", PoisonPill.getInstance(), ""), "WatchDog");
        watchdog = system.actorOf(ClusterSingletonProxy.defaultProps("/user/WatchDog/WatchDogSingleton", ""), "WatchDogProxy");
    }

    private void shardStreams() {
        int numberOfNodes = 3;
        int factorOfTen = 10;
        int idealNumberOfShards = numberOfNodes * factorOfTen;

        ClusterSharding.get(system).start("Streams",
                                          StreamSupervisor.props(),
                                          new MessageExtractor(idealNumberOfShards),
                                          new NonRebalancingLeastShardAllocationStrategy());
    }

    private void start() {
        createTwitterStream();

//        Config config = configuration.withFallback(ConfigFactory.parseFile(new File("configuration/feeds/twitter/application.conf")));
//
//        Stream facebookStream = new Stream("Streams", 2L, StreamActor.props(config, new FacebookStreamManagerBuilder()));
//        watchdog.tell(Persistent.create(facebookStream), ActorRef.noSender());
//
//        Stream googleStream = new Stream("Streams", 3L, StreamActor.props(config, new GoogleStreamManagerBuilder()));
//        watchdog.tell(Persistent.create(googleStream), ActorRef.noSender());
    }

    private void createTwitterStream() {
        Config config = configuration.getConfig("stream");//.withFallback(ConfigFactory.parseFile(new File("configuration/feeds/twitter/application.conf")));

        //*************** the same entryId cannot be associated with 2 different shardIds but 2 different entryIds can be associated with the same shardId.
        Stream twitterStream = new Stream("Streams", 1L, StreamActor.props("twitter", new TwitterStreamManagerBuilder()));
        watchdog.tell(Persistent.create(twitterStream), ActorRef.noSender());
    }
}
