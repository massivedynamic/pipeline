package massivedynamic.pipeline.test.io;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.nio.charset.StandardCharsets;

/**
 * @author lmedina
 */
public class ResourceFileLoader {
    private static final Logger LOGGER = LoggerFactory.getLogger(ResourceFileLoader.class);

    public String loadString(Class clazz, String fileName) {
        try {
            return FileUtils.readFileToString(new File(clazz.getResource("/" + fileName).toURI()), StandardCharsets.UTF_8.name());
        } catch (Exception e) {
            LOGGER.error(String.format("Unable to read file: %s.", fileName), e);
            return StringUtils.EMPTY;
        }
    }
}
