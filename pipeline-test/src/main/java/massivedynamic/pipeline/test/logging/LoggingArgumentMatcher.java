package massivedynamic.pipeline.test.logging;

import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.classic.spi.LoggingEvent;
import org.apache.commons.lang.Validate;
import org.mockito.ArgumentMatcher;

/**
 * @author lmedina
 */
public class LoggingArgumentMatcher extends ArgumentMatcher<ILoggingEvent> {
    private String message;

    public LoggingArgumentMatcher(String message) {
        Validate.notNull(message, "message must not be null.");

        this.message = message;
    }

    @Override
    public boolean matches(Object argument) {
        return ((LoggingEvent) argument).getFormattedMessage().contains(message);
    }
}
