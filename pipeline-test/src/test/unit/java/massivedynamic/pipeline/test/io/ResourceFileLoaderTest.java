package massivedynamic.pipeline.test.io;

import ch.qos.logback.classic.Logger;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.Appender;
import org.apache.commons.lang.StringUtils;
import massivedynamic.pipeline.test.logging.LoggingArgumentMatcher;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.slf4j.LoggerFactory;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.argThat;
import static org.mockito.Mockito.verify;

/**
 * @author lmedina
 */
@RunWith(MockitoJUnitRunner.class)
public class ResourceFileLoaderTest {
    @Mock
    private Appender<ILoggingEvent> appender;

    private ResourceFileLoader resourceFileLoader;

    @Before
    public void setUp() {
        ((Logger) LoggerFactory.getLogger(Logger.ROOT_LOGGER_NAME)).addAppender(appender);

        resourceFileLoader = new ResourceFileLoader();
    }

    @Test
    public void loadStringFromFile() {
        String actualString = resourceFileLoader.loadString(ResourceFileLoaderTest.class, "test-file");

        assertEquals("String value did not match expected value.", "This is a test.", actualString);
    }

    @Test
    public void loadNonExistentFile() {
        String actualString = resourceFileLoader.loadString(ResourceFileLoaderTest.class, "does-not-exist.txt");

        assertEquals("String value did not match expected value.", StringUtils.EMPTY, actualString);
        verify(appender).doAppend(argThat(new LoggingArgumentMatcher("Unable to read file: does-not-exist.")));
    }
}
