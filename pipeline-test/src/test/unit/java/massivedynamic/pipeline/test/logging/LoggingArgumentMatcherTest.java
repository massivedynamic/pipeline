package massivedynamic.pipeline.test.logging;

import ch.qos.logback.classic.spi.LoggingEvent;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;

/**
 * @author lmedina
 */
public class LoggingArgumentMatcherTest {
    private static final String TEST_MESSAGE = "Test message.";

    private LoggingArgumentMatcher loggingArgumentMatcher;

    @Before
    public void setUp() {
        loggingArgumentMatcher = new LoggingArgumentMatcher(TEST_MESSAGE);
    }

    @Test
    public void validConstructor() {
        assertNotNull("loggingArgumentMatcher must not be null.", loggingArgumentMatcher);
    }

    @Test(expected = IllegalArgumentException.class)
    public void constructorThrowsIllegalArgumentException() {
        new LoggingArgumentMatcher(null);
    }

    @Test
    public void loggedMessageMatches() {
        LoggingEvent loggingEvent = mock(LoggingEvent.class);
        doReturn(TEST_MESSAGE).when(loggingEvent).getFormattedMessage();

        boolean match = loggingArgumentMatcher.matches(loggingEvent);

        assertTrue("match must be true.", match);
    }

    @Test
    public void loggedMessageDoesNotMatch() {
        LoggingEvent loggingEvent = mock(LoggingEvent.class);
        doReturn("Different test message.").when(loggingEvent).getFormattedMessage();

        boolean match = loggingArgumentMatcher.matches(loggingEvent);

        assertFalse("match must be false.", match);
    }
}
