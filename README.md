# Pipeline #

Pipeline is an ongoing effort to develop a reactive, large-scale, data ingestion, pipeline system based on the principles detailed in the [Reactive Manifesto](http://www.reactivemanifesto.org/) through the use of the [Akka](http://akka.io/) framework.

# The Reactive Manifesto - What is it and why is it important? #

A few years ago a large application had tens of servers, seconds of response time, hours of offline maintenance, and gigabytess of data. Applications requirements, however, have changed dramatically in recent years. There is now a new demand for clusters with thousands of multi-core processors, millisecond response times, 100% up-time, and petabytes of data which has led to a new paradigm of reactive applications. 

## The Need To Go Reactive ##

Reactive applications satisfy these new demands. They deliver responsive user experiences, backed by a scalable and resilient application stack, ready to be deployed on multi-core and cloud architectures. Now, reactive as defined in the dictionary means, “*readily responsive to a stimulus*”, which for us translates into having the ability to react to events, load, failure, users, etc. There is something called the Reactive Manifesto and it defines the four main characteristics of a reactive application as being event-driven, scalable, resilient, and responsive.

### Event-Driven ###

When you are event-driven, your applications shouldn't have to care how events are propagated throughout your system. This means that you focus on the content of the communication and not on the communication itself. Events are sent/received in an asynchronous and non-blocking fashion which means not only that applications are highly concurrent but also that they have the ability to make continuous progress and not waste resources waiting while still remaining responsive.

### Scalable ###

Being scalable means having elasticity which makes it easy to scale up or down on demand without redesigning or rewriting an application. More importantly though, this enables you to minimize operation costs and manage risk through a pay-for-what-you-use model. One of the most important factors in being scalable is having location transparency. This means that in terms of implementation, there should be no difference between scaling up by using multiple threads, scaling up by using multiple cores, or scaling out by using more machines in a data center. This is typically achieved through configuration or adaptive run-time algorithms that respond to application usage.

### Resilient ###

Downtime is one of the most damaging issues that can happen to a company. It usually leaves a hole in the revenue stream, leads to unhappy customers, and creates a poor reputation. Surprisingly though, application resilience is a requirement that is largely ignored or retrofitted using ad-hoc techniques. Resilience should not be an afterthought but should be a part of the design from the very beginning. By making failure a first-class construct in your design, you are able to provide the means to react to it and manage it through applications that are highly tolerant to failure through self-healing. The loose coupling in the event-driven model provides component isolation in which failures can be captured. This creates a system where the business logic remains clean and separated from the handling of the unexpected.

### Responsive ###

Responsiveness is what ultimately interconnects everything to form a cohesive whole. By building on an event-driven foundation, applications are able to provide the tools needed to ensure scalability and resilience while at the same time supporting responsive interactions. Reactive applications represent a balance between these four characteristics and it’s something that a rapidly increasing number of systems are beginning to adopt. 

# Technology Stack #
* [Java 8](http://www.oracle.com/technetwork/java/javase/overview/java8-2100321.html)
* [Akka](http://akka.io/)
* [Typesafe config](https://github.com/typesafehub/config)
* [LogBack](http://logback.qos.ch/)
* [Gradle](http://www.gradle.org/)

# Current State #

The main part of the system that has been implemented so far are the streaming components which are capable of protecting themselves against failure by ensuring quick recovery and minimal downtime through Akka's clustering feature.

### What does it look like? ###

![Akka - Overview & Findings.jpg](https://bitbucket.org/repo/o7z5r9/images/3491425746-Akka%20-%20Overview%20%26%20Findings.jpg)

At the very top of the actor hierarchy is a StreamSupervisor that oversees the creation of the StreamActor which is where the actual stream runs. The StreamActor is pluggable and could be replaced with a different stream implementation.

The ValveActor is what connects directly to the data source and pulls data in. 

This data is then routed to the PumpActor which moves it forward to the HydrantActor. In addition to moving data, the Pump actor sits in an ideal location that could be used to keep track of message rates as well as to control the data flow.

The HydrantActor is where data will get outputted in 2 different ways. The first is by sending it to another actor which write it into our central queue system. The second is by publishing it to a topic that other actors can subscribe using Akka's distributed Publish Subscribe service. This is something that could be used by another actor to hook into the and retrieve the raw data stream.

![Akka - Overview & Findings (1).jpg](https://bitbucket.org/repo/o7z5r9/images/413269594-Akka%20-%20Overview%20%26%20Findings%20%281%29.jpg)

Now, I went into this project with the mindset of designing the system to be very defensive which meant supporting streams that were resilient to failure. In order to do this, I used sharding (another feature with Akka clustering) not to parallelize the functionality of the stream but to make sure that it could get restarted if the node it was running on went down.

One of the caveats about sharding is that a sharded actor won't actually get started until a message is sent to it. This was not good if I expected my streams to automatically start-up on node failure. 

Because of this, I also implemented a WatchDog singleton that constantly pings all of the streams in the system to ensure they are brought back to life whenever they get restarted.

![Akka - Overview & Findings (2).jpg](https://bitbucket.org/repo/o7z5r9/images/2325223993-Akka%20-%20Overview%20%26%20Findings%20%282%29.jpg)

In the cluster, streams are sharded in such a way that they get distributed evenly to other machines favoring those that don’t already have a stream running.

In addition, each of the routers, the PumpRouter and the HydrantRouter are cluster-aware which means that they will take advantage of the fact that you’re using multiple machines and split themselves up. These routers may get changed in the future to be sharded actors.

# To-Do's #

* Add monitoring and metrics to track the system inside and out.
* Make the application highly modularized and configuration-driven to allow for customization and live deployments. 
* Continue the use of good design principles to future-proof the system as much as possible and make the addition of new data feeds or processing units a simple matter of implementing a few common interfaces.
* Implement centralized messaging system (RabbitMQ, Kafka, etc).
* Add the ability to make the project packageable into an rpm and dpkg (https://github.com/nebula-plugins/gradle-ospackage-plugin).

# How To Run #

## Building The Application ##

### Requirements ###

* Regardless of whether you're using the runnables given in the pipeline.tar in the Downloads section or you're building the project yourself, you will need to use Java 8 at a minimum. Java 8's new constructs are not heavily integrated into the application except for a few lambdas that are used here and there. Version 8 was chosen primarily to keep up to date with the latest Java release.
* This application uses Gradle as its build system. If you're going to build the application yourself, you will need to have Gradle (version 1.3 at the minimum) installed on your local environment. Thanks to the use of the Gradle wrapper, you can easily switch between different versions of Gradle without having to update your local installation every time by simply editing the *build.gradle* file in the project's root directory.
* This application was developed using IntelliJ as it offers a wide variety of features and plugins that make it extremely easy and convenient to work with. As such, IntelliJ is the recommended IDE to use for this project. The next few sections will assume that the user is working with IntelliJ. 
* The Gradle plugin for IntelliJ.

### Steps ###

1. Download a copy of the project onto your local environment and import it into IntelliJ by selecting the build.gradle file. Click through the steps with all of the defaults selected.
2. Once the project has been imported, open up the side tab for the Gradle plugin. Here it should show a list of tasks if you click on the arrow to the side of the entry called "pipeline" under the "All tasks" section of the tab. 
3. In order to generate the same executable files as the ones available in the "Downloads" section, run the task called "installApp". This will use the Gradle "application" plugin which extends Gradle with common application related tasks. After running it, you should see a "BUILD SUCCESSFUL" message.
4. Go to /pipeline/pipeline-node/build/install which is where you will find the newly created executables. From here, follow the steps detailed below.

## Running The Application ##

These are the instructions for running the application. As mentioned above, this application is built on top of Akka's clustering service. As such, you're able to simulate running different nodes of the cluster locally as detailed below.

### Requirements ###

Java 8

### Configurations ###

* Inside */pipeline/bin/configuration* you will find all of the configurations of the application which are based on the [Typesafe config library](https://github.com/typesafehub/config). 

* Inside */logging* is the *logback.xml* file which is used to configure [LogBack](http://logback.qos.ch/), the logging implementation used by Pipeline. LogBack will scan for changes in its configuration file and automatically reconfigure itself when the configuration file is modified. By default, the file is scanned for changes once every minute.

* Inside */feeds/twitter* you will find the *application.conf* file which is used to setup and configure the Twitter feed. The API keys provided there are safe to use without worry of privacy concerns. Feel free to replace them with your own set of keys.

* Inside */akka* you will find another *application.conf* file which is used to configure the Akka system. To learn more about Akka's configuration, go [here](http://doc.akka.io/docs/akka/2.3.3/general/configuration.html).

### Steps ###

1. Get the runnables onto your local environment. You can find a copy of these in the .tar file in the Downloads section. Alternatively, you can also build the application yourself in order to obtain these runnables.
 
2. Open up the Akka configuration file, */pipeline/bin/configuration/akka/application.conf*. Towards the bottom of the file, under the configuration path "akka.remote.netty.tcp.port", you will find a port number. While an Akka cluster would normally be deployed across many different machines, we can use the port number to simulate distribution when running the cluster on a single machine. Here make sure that the port number is set to the same value as the port in the first element of the "akka.cluster.seed-nodes" list which is defined in the same file (it should be 2550). The node configured as the first element in the "akka.cluster.seed-nodes" list must be booted up first when initially starting a cluster, otherwise no other node will be able to join the cluster.

3. Open up a terminal, go into */pipeline/bin*, and run one of the 2 executables that corresponds to your operating system ("pipeline" for Linux and OSX, and "pipeline.bat" for Windows). When the application starts up, you will see logging that is created by Akka as it initializes the cluster. After a very short period of time you will then see the Twitter feed log its successful connection followed by the Twitter stream.

4. When the application is started up, it will create a log file under */pipeline/bin/log* where you can take a closer look at the logged events to see the different actions taking place within the cluster.

### Now what? ###

So you got the application running, that's great! Now what? Well, start up more nodes! To do this simply change the port value of the Akka configuration file to a different value than those of the nodes that are already running, open up a new terminal window, and run the executable. 

Very soon after the new node starts up, you will see this second terminal window start outputting the Twitter stream much like the window of the other nodes. This is because the application is using cluster-aware routers to distribute the work of outputting the stream and it takes advantage of new resources (ie. new machines) that are added to the cluster. See above for more details about how the application is currently designed. Feel free to keep adding as many nodes as you want.

Once you have a cluster of 2 or more nodes running, go ahead and shutdown the first node that is running the stream (a simple CTRL+C should do it). You will notice the streaming output for the rest of the nodes stop along with logging from Akka as the cluster detects and responds to the "failure" of the node that was shutdown. After a short while, you will see the Twitter feed start back up on one of the active nodes in the cluster and the stream will continue as normal. 

From here on end, you can go ahead and play around with starting up new nodes and bringing them down again and watch as the Twitter feed bounces from node to node.