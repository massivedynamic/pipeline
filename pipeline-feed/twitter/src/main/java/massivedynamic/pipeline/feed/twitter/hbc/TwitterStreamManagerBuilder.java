package massivedynamic.pipeline.feed.twitter.hbc;

import massivedynamic.pipeline.core.stream.StreamManager;
import massivedynamic.pipeline.core.stream.StreamManagerBuilder;

/**
 * @author lmedina
 */
public class TwitterStreamManagerBuilder extends StreamManagerBuilder<String> {
    @Override
    public StreamManager build() {
        return new TwitterStreamManager(new ClientFactory(configuration, messageQueue).create());
    }

//    @Override
//    public StreamManager build() {
//        return new TwitterStreamManager(new Client(messageQueue));
//    }
}
