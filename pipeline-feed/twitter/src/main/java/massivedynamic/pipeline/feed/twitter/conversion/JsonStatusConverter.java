package massivedynamic.pipeline.feed.twitter.conversion;

import org.apache.commons.lang.Validate;
import massivedynamic.pipeline.core.data.conversion.Converter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import twitter4j.Status;
import twitter4j.TwitterException;
import twitter4j.TwitterObjectFactory;

/**
 * @author lmedina
 */
public class JsonStatusConverter implements Converter<String, Status> {
    private static final Logger LOGGER = LoggerFactory.getLogger(JsonStatusConverter.class);

    @Override
    public Status convert(String json) {
        Validate.notNull(json, "json must not be null.");

        return getStatus(json);
    }

    private Status getStatus(String json) {
        Object object = getTwitterObject(json);

        if (object instanceof Status) {
            return (Status) object;
        } else {
            return null;
        }
    }

    private Object getTwitterObject(String json) {
        try {
            return TwitterObjectFactory.createObject(json);
        } catch (TwitterException e) {
            LOGGER.warn("A problem was encountered while creating Twitter4J object out of JSON.");
            return null;
        }
    }
}
