package massivedynamic.pipeline.feed.twitter.conversion;

import org.apache.commons.lang.Validate;
import massivedynamic.pipeline.core.data.Pipeline;
import massivedynamic.pipeline.core.data.conversion.Converter;
import twitter4j.Status;

/**
 * @author lmedina
 */
public class JsonMessageConverter implements Converter<String, Pipeline.Message> {
    private Converter<String, Status> jsonStatusConverter;
    private Converter<Status, Pipeline.Message> statusMessageConverter;

    public JsonMessageConverter(Converter<String, Status> jsonStatusConverter, Converter<Status, Pipeline.Message> statusMessageConverter) {
        this.jsonStatusConverter = jsonStatusConverter;
        this.statusMessageConverter = statusMessageConverter;
    }

    @Override
    public Pipeline.Message convert(String json) {
        Validate.notNull(json, "json must not be null.");

        return statusMessageConverter.convert(jsonStatusConverter.convert(json));
    }
}
