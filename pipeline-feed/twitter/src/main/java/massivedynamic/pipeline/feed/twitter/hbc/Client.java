package massivedynamic.pipeline.feed.twitter.hbc;

import com.twitter.hbc.SitestreamController;
import com.twitter.hbc.core.StatsReporter;
import com.twitter.hbc.core.endpoint.StreamingEndpoint;
import org.apache.commons.lang.Validate;

import java.util.concurrent.BlockingQueue;

/**
 * @author lmedina
 */
public class Client implements com.twitter.hbc.core.Client {
    private BlockingQueue<String> messageQueue;

    public Client(BlockingQueue<String> messageQueue) {
        Validate.notNull(messageQueue, "messageQueue must not be null.");

        this.messageQueue = messageQueue;
    }

    private long previousTime = 0L;

    @Override
    public void connect() {
        while (true) {
            long currentTime = System.currentTimeMillis();
            if (currentTime - previousTime >= 250) {
                messageQueue.add("twitter");
                previousTime = currentTime;
            }
        }
    }

    @Override
    public void reconnect() {

    }

    @Override
    public void stop() {

    }

    @Override
    public void stop(int waitMillis) {

    }

    @Override
    public boolean isDone() {
        return false;
    }

    @Override
    public String getName() {
        return null;
    }

    @Override
    public StreamingEndpoint getEndpoint() {
        return null;
    }

    @Override
    public SitestreamController createSitestreamController() {
        return null;
    }

    @Override
    public StatsReporter.StatsTracker getStatsTracker() {
        return null;
    }
}
