package massivedynamic.pipeline.feed.twitter.conversion;

import org.apache.commons.lang.StringUtils;
import massivedynamic.pipeline.core.data.Pipeline;
import massivedynamic.pipeline.core.data.conversion.Converter;
import org.joda.time.DateTime;
import org.joda.time.DateTimeUtils;
import twitter4j.*;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author lmedina
 */
public class StatusMessageConverter implements Converter<Status, Pipeline.Message> {
    private static final String TWITTER_DOT_COM = "http:/twitter.com/";
    private static final String STATUS = "/status/";
    private static final String SOURCE_REPLACEMENT_PATTERN = "<.*?>";

    @Override
    public Pipeline.Message convert(Status status) {
        if (null == status) {
            return Pipeline.Message.getDefaultInstance();
        } else {
            return Pipeline.Message.newBuilder()
                    .setHeader(getHeader())
                    .setBody(getBody(status))
                    .build();
        }
    }

    private Pipeline.Header getHeader() {
        return Pipeline.Header.newBuilder()
                .setAcquiredTime(DateTimeUtils.currentTimeMillis())
                .build();
    }

    private Pipeline.Body getBody(Status status) {
        return Pipeline.Body.newBuilder()
                .setId(getId(status))
                .setUrl(getMessageUrl(status))
                .setText(status.getText())
                .setAuthor(getAuthor(status))
                .setFeed(Pipeline.Feed.TWITTER)
                .setPublishedTime(getPublishedTime(status))
                .setLanguage(status.getLang())
                .setSource(getSource(status))
                .setGeoLocation(getGeoLocation(status))
                .addAllUrls(getUrls(status))
                .setReply(getReply(status))
                .setExtension(Pipeline.Twitter.fields, getTwitterFields(status))
                .build();
    }

    private String getId(Status status) {
        return Long.toString(status.getId());
    }

    private String getMessageUrl(Status status) {
        return String.format("%s%s%d", getProfileUrl(status), STATUS, status.getId());
    }

    private Pipeline.Author getAuthor(Status status) {
        return Pipeline.Author.newBuilder()
                .setId(Long.toString(status.getUser().getId()))
                .setName(status.getUser().getName())
                .setUserName(status.getUser().getScreenName())
                .setImageUrl(status.getUser().getBiggerProfileImageURL())
                .setProfileUrl(getProfileUrl(status))
                .build();
    }

    private String getProfileUrl(Status status) {
        return String.format("%s%s", TWITTER_DOT_COM, status.getUser().getScreenName());
    }

    private long getPublishedTime(Status status) {
        return new DateTime(status.getCreatedAt()).getMillis();
    }

    private String getSource(Status status) {
        if (StringUtils.isEmpty(status.getSource())) {
            return StringUtils.EMPTY;
        } else {
            return status.getSource().replaceAll(SOURCE_REPLACEMENT_PATTERN, StringUtils.EMPTY);
        }
    }

    private Pipeline.GeoLocation getGeoLocation(Status status) {
        return Pipeline.GeoLocation.newBuilder()
                .setCoordinates(getCoordinates(status))
                .build();
    }

    private Pipeline.GeoLocation.Coordinates getCoordinates(Status status) {
        if (null == status.getGeoLocation()) {
            return Pipeline.GeoLocation.Coordinates.getDefaultInstance();
        } else {
            return Pipeline.GeoLocation.Coordinates.newBuilder()
                    .setLatitude(status.getGeoLocation().getLatitude())
                    .setLongitude(status.getGeoLocation().getLongitude())
                    .build();
        }
    }

    private List<String> getUrls(Status status) {
        return Arrays.asList(status.getURLEntities())
                .stream()
                .map(URLEntity::getText)
                .collect(Collectors.toList());
    }

    private Pipeline.Reply getReply(Status status) {
        return Pipeline.Reply.newBuilder()
                .setIsReply(getIsReply(status))
                .setOriginalMessageId(getOriginalMessageId(status))
                .build();
    }

    private boolean getIsReply(Status status) {
        return (-1 != status.getInReplyToStatusId());
    }

    private String getOriginalMessageId(Status status) {
        if (getIsReply(status)) {
            return Long.toString(status.getInReplyToStatusId());
        } else {
            return StringUtils.EMPTY;
        }
    }

    private Pipeline.Twitter getTwitterFields(Status status) {
        return Pipeline.Twitter.newBuilder()
                .addAllHashtags(getHashtags(status))
                .addAllMentions(getMentions(status))
                .addAllMediaUrls(getMediaUrls(status))
                .setRetweet(getRetweet(status))
                .build();
    }

    private List<String> getHashtags(Status status) {
        return Arrays.asList(status.getHashtagEntities())
                .stream()
                .map(HashtagEntity::getText)
                .collect(Collectors.toList());
    }

    private List<String> getMentions(Status status) {
        return Arrays.asList(status.getUserMentionEntities())
                .stream()
                .map(UserMentionEntity::getText)
                .collect(Collectors.toList());
    }

    private List<String> getMediaUrls(Status status) {
        return Arrays.asList(status.getMediaEntities())
                .stream()
                .map(MediaEntity::getMediaURL)
                .collect(Collectors.toList());
    }

    private Pipeline.Twitter.Retweet getRetweet(Status status) {
        return Pipeline.Twitter.Retweet.newBuilder()
                .setIsRetweet(getIsRetweet(status))
                .setOriginalTweetId(getOriginalRetweetId(status))
                .build();
    }

    private boolean getIsRetweet(Status status) {
        return null != status.getRetweetedStatus();
    }

    private String getOriginalRetweetId(Status status) {
        if (getIsRetweet(status)) {
            return Long.toString(status.getRetweetedStatus().getId());
        } else {
            return StringUtils.EMPTY;
        }
    }
}
