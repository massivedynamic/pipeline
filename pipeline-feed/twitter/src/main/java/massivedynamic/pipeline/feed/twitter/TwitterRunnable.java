package massivedynamic.pipeline.feed.twitter;

import massivedynamic.pipeline.core.data.Pipeline;
import massivedynamic.pipeline.core.data.conversion.Converter;
import massivedynamic.pipeline.core.io.Reader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * @author lmedina
 */
public class TwitterRunnable implements Runnable {
    private static final Logger LOGGER = LoggerFactory.getLogger(TwitterRunnable.class);

    private static final long TIMEOUT = 5;

    private Converter<String, Pipeline.Message> converter;
    private Reader<String> reader;
    private BlockingQueue<String> messageQueue;

    private AtomicBoolean shutdown = new AtomicBoolean(false);

    public TwitterRunnable(Reader<String> reader, Converter<String, Pipeline.Message> converter, BlockingQueue<String> messageQueue) {
        this.reader = reader;
        this.converter = converter;
        this.messageQueue = messageQueue;
    }

    public void shutdown() {
        shutdown.set(true);
    }

    @Override
    public void run() {
//        while (shouldContinue()) {
//            String json = reader.read();
//
//            if (null != json) {
//                Pipeline.Message message = converter.convert(json);
//
//                if (message.isInitialized()) {
//                    System.out.println(message);
//                }
//            }

//        new ForkJoinPool(3).submit(() ->
//            Stream.generate(new BlockingQueueSupplier<>(messageQueue, 5))
//                  .filter(x -> x != null)
//                  .parallel()
//                  .map(converter::convert)
//                  .sequential()
//                  .filter(Pipeline.Message::isInitialized)
//                  .forEach(System.out::println)
//        );
//        }
    }

    private boolean shouldContinue() {
        return !shutdown.get();
    }
}
