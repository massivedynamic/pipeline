package massivedynamic.pipeline.feed.twitter.configuration;

/**
 * @author lmedina
 */
public class TwitterStreamConfiguration {
    public static class Authentication {
        public static final String ACCESS_TOKEN = "authentication.accessToken";
        public static final String ACCESS_TOKEN_SECRET = "authentication.accessTokenSecret";
        public static final String API_KEY = "authentication.apiKey";
        public static final String API_SECRET = "authentication.apiSecret";
    }

    public static final String DATA_STREAM = "dataStream";

    public static class HBC {
        public static final String CLIENT_NAME = "hbc.clientName";
        public static final String CONNECTION_RETRIES = "hbc.connectionRetries";
        public static final String GZIP_ENABLED = "hbc.gzipEnabled";
        public static final String PARTITIONS = "hbc.partitions";
        public static final String STALL_WARNINGS = "hbc.stallWarnings";
    }
}
