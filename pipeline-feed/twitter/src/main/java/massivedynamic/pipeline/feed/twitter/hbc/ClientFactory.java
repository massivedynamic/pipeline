package massivedynamic.pipeline.feed.twitter.hbc;

import com.google.common.collect.Sets;
import com.twitter.hbc.BasicReconnectionManager;
import com.twitter.hbc.ClientBuilder;
import com.twitter.hbc.ReconnectionManager;
import com.twitter.hbc.core.Client;
import com.twitter.hbc.core.Constants;
import com.twitter.hbc.core.HttpHosts;
import com.twitter.hbc.core.endpoint.DefaultStreamingEndpoint;
import com.twitter.hbc.core.endpoint.StatusesFirehoseEndpoint;
import com.twitter.hbc.core.endpoint.StatusesSampleEndpoint;
import com.twitter.hbc.core.endpoint.StreamingEndpoint;
import com.twitter.hbc.core.processor.StringDelimitedProcessor;
import com.twitter.hbc.httpclient.auth.Authentication;
import com.twitter.hbc.httpclient.auth.OAuth1;
import com.typesafe.config.Config;
import massivedynamic.pipeline.feed.twitter.configuration.TwitterStreamConfiguration;
import org.apache.commons.lang.Validate;
import massivedynamic.pipeline.core.ioc.AbstractFactory;

import java.util.concurrent.BlockingQueue;

/**
 * @author lmedina
 */
public class ClientFactory implements AbstractFactory<Client> {
    private Config configuration;

    private BlockingQueue<String> messageQueue;

    public ClientFactory(Config configuration, BlockingQueue<String> messageQueue) {
        Validate.notNull(configuration, "configuration must not be null.");
        Validate.notNull(messageQueue, "messageQueue must not be null.");

        this.configuration = configuration;
        this.messageQueue = messageQueue;
    }

    @Override
    public Client create() {
        return new ClientBuilder().authentication(createAuthentication())
                                  .endpoint(createStreamingEndpoint())
                                  .gzipEnabled(configuration.getBoolean(TwitterStreamConfiguration.HBC.GZIP_ENABLED))
                                  .hosts(HttpHosts.STREAM_HOST)
                                  .name(configuration.getString(TwitterStreamConfiguration.HBC.CLIENT_NAME))
                                  .processor(new StringDelimitedProcessor(messageQueue))
                                  .reconnectionManager(createReconnectionManager())
                                  .build();
    }

    private Authentication createAuthentication() {
        return new OAuth1(configuration.getString(TwitterStreamConfiguration.Authentication.API_KEY),
                          configuration.getString(TwitterStreamConfiguration.Authentication.API_SECRET),
                          configuration.getString(TwitterStreamConfiguration.Authentication.ACCESS_TOKEN),
                          configuration.getString(TwitterStreamConfiguration.Authentication.ACCESS_TOKEN_SECRET));
    }

    private StreamingEndpoint createStreamingEndpoint() {
        if (configuration.getString(TwitterStreamConfiguration.DATA_STREAM).equals("firehose")) {
            return createFirehoseEndpoint();
        } else {
            return createGardenhoseEndpoint();
        }
    }

    private StreamingEndpoint createFirehoseEndpoint() {
        DefaultStreamingEndpoint endpoint = getConfiguredEndpoint(new StatusesFirehoseEndpoint());
        ((StatusesFirehoseEndpoint) endpoint).partitions(Sets.newTreeSet(configuration.getIntList(TwitterStreamConfiguration.HBC.PARTITIONS)));

        return endpoint;
    }

    private StreamingEndpoint createGardenhoseEndpoint() {
        return getConfiguredEndpoint(new StatusesSampleEndpoint());
    }

    private DefaultStreamingEndpoint getConfiguredEndpoint(DefaultStreamingEndpoint endpoint) {
        endpoint.setBackfillCount(Constants.MAX_BACKOFF_COUNT);
        endpoint.stallWarnings(configuration.getBoolean(TwitterStreamConfiguration.HBC.STALL_WARNINGS));
        endpoint.filterLevel(Constants.FilterLevel.None);

        return endpoint;
    }

    private ReconnectionManager createReconnectionManager() {
        return new BasicReconnectionManager(configuration.getInt(TwitterStreamConfiguration.HBC.CONNECTION_RETRIES));
    }
}
