package massivedynamic.pipeline.feed.twitter;

import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import massivedynamic.pipeline.core.data.Pipeline;
import massivedynamic.pipeline.core.data.conversion.Converter;
import massivedynamic.pipeline.core.io.BlockingQueueReader;
import massivedynamic.pipeline.core.io.Reader;
import massivedynamic.pipeline.core.stream.StreamManager;
import massivedynamic.pipeline.feed.twitter.conversion.JsonMessageConverter;
import massivedynamic.pipeline.feed.twitter.conversion.JsonStatusConverter;
import massivedynamic.pipeline.feed.twitter.conversion.StatusMessageConverter;
import massivedynamic.pipeline.feed.twitter.hbc.ClientFactory;
import massivedynamic.pipeline.feed.twitter.hbc.TwitterStreamManager;

import java.io.File;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * @author lmedina
 */
public class Twitter {
    private Config configuration;

    private BlockingQueue<String> messageQueue;
    private Reader<String> reader;
    private StreamManager twitterConnection;
    private Converter<String, Pipeline.Message> converter;

    private TwitterRunnable runnable;

    private static Twitter twitter = new Twitter();

    public static void main(String[] args) {
        twitter.init();
        twitter.start();

        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        twitter.stop();

        System.exit(0);
    }

    private void init() {
        configuration = ConfigFactory.parseFile(new File("configuration/feeds/twitter.conf"));
        messageQueue = new LinkedBlockingQueue<>();
        reader = new BlockingQueueReader<>(messageQueue);
        twitterConnection = new TwitterStreamManager(new ClientFactory(configuration, messageQueue).create());
        converter = new JsonMessageConverter(new JsonStatusConverter(), new StatusMessageConverter());

        runnable = new TwitterRunnable(reader, converter, messageQueue);
    }

    private void start() {
        twitterConnection.connect();
        processMessages();
    }

    private void stop() {
        twitterConnection.disconnect();
        shutdownProcessingLoop();
    }

    private void processMessages() {
        Executors.newSingleThreadExecutor().submit(runnable);
    }

    private void shutdownProcessingLoop() {
        if (null != runnable) {
            runnable.shutdown();
        }
    }
}
