package massivedynamic.pipeline.feed.twitter.conversion;

import massivedynamic.pipeline.core.data.Pipeline;
import massivedynamic.pipeline.core.data.conversion.Converter;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import twitter4j.Status;

import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;

/**
 * @author lmedina
 */
@RunWith(MockitoJUnitRunner.class)
public class JsonMessageConverterTest {
    @Mock
    private Status status;
    @Mock
    private Converter<String, Status> jsonStatusConverter;
    @Mock
    private Converter<Status, Pipeline.Message> statusMessageConverter;

    private Converter<String, Pipeline.Message> jsonMessageConverter;

    @Before
    public void setUp() {
        jsonMessageConverter = new JsonMessageConverter(jsonStatusConverter, statusMessageConverter);
    }

    @Test
    public void convertJson() {
        String json = "test json";
        doReturn(status).when(jsonStatusConverter).convert(json);

        jsonMessageConverter.convert(json);

        verify(jsonStatusConverter).convert(json);
        verify(statusMessageConverter).convert(status);
    }

    @Test(expected = IllegalArgumentException.class)
    public void convertNullJson() {
        jsonMessageConverter.convert(null);
    }
}
