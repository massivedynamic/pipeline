package massivedynamic.pipeline.feed.twitter.hbc;

import ch.qos.logback.classic.Logger;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.Appender;
import com.twitter.hbc.core.Client;
import massivedynamic.pipeline.test.logging.LoggingArgumentMatcher;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.slf4j.LoggerFactory;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.*;

/**
 * @author lmedina
 */
@RunWith(MockitoJUnitRunner.class)
public class TwitterStreamManagerTest {
    @Mock
    private Appender<ILoggingEvent> appender;
    @Mock
    private Client client;

    private TwitterStreamManager twitterStreamManager;

    @Before
    public void setUp() {
        ((Logger) LoggerFactory.getLogger(Logger.ROOT_LOGGER_NAME)).addAppender(appender);

        twitterStreamManager = new TwitterStreamManager(client);
    }

    @Test
    public void createConnectionManagerWithValidConstructor() {
        assertNotNull("connectionManager must not be null.", twitterStreamManager);
    }

    @Test (expected = IllegalArgumentException.class)
    public void createConnectionManagerWithNullClient() {
        new TwitterStreamManager(null);
    }

    @Test
    public void connectClientSuccess() {
        twitterStreamManager.connect();

        verify(client).connect();
        verify(appender).doAppend(argThat(new LoggingArgumentMatcher("Connected to the data stream.")));
    }

    @Test
    public void connectClientFailure() throws Exception {
        doThrow(new RuntimeException()).when(client).connect();

        twitterStreamManager.connect();

        verify(client).connect();
        verify(appender).doAppend(argThat(new LoggingArgumentMatcher("Failed to connect to the data stream.")));
    }

    @Test
    public void connectAlreadyConnectedClient() {
        twitterStreamManager.connect();

        twitterStreamManager.connect();

        verify(client).connect();
        verify(appender).doAppend(argThat(new LoggingArgumentMatcher("Connected to the data stream.")));
        verify(appender).doAppend(argThat(new LoggingArgumentMatcher("Already connected to the data stream.")));
    }

    @Test
    public void connectDisconnectedClient() {
        twitterStreamManager.connect();
        twitterStreamManager.disconnect();

        twitterStreamManager.connect();

        verify(client).connect();
        verify(client).stop();
        verify(appender).doAppend(argThat(new LoggingArgumentMatcher("Connected to the data stream.")));
        verify(appender).doAppend(argThat(new LoggingArgumentMatcher("Permanently disconnected from the data stream.")));
        verify(appender).doAppend(argThat(new LoggingArgumentMatcher("Data stream has been permanently disconnected. Unable to connect.")));
    }

    @Test
    public void reconnectClient() {
        twitterStreamManager.connect();

        twitterStreamManager.reconnect();

        verify(client).connect();
        verify(client).reconnect();
        verify(appender).doAppend(argThat(new LoggingArgumentMatcher("Connected to the data stream.")));
        verify(appender).doAppend(argThat(new LoggingArgumentMatcher("Reconnected to the data stream.")));
    }

    @Test
    public void reconnectClientFailure() {
        twitterStreamManager.connect();
        doThrow(new RuntimeException()).when(client).reconnect();

        twitterStreamManager.reconnect();

        verify(client).connect();
        verify(client).reconnect();
        verify(appender).doAppend(argThat(new LoggingArgumentMatcher("Connected to the data stream.")));
        verify(appender).doAppend(argThat(new LoggingArgumentMatcher("Failed to reconnect to the data stream.")));
    }

    @Test
    public void reconnectUnconnectedClient() {
        twitterStreamManager.reconnect();

        verify(client, never()).reconnect();
        verify(appender).doAppend(argThat(new LoggingArgumentMatcher("Not previously connected to the data stream. Unable to reconnect.")));
    }

    @Test
    public void reconnectDisconnectedClient() {
        twitterStreamManager.connect();
        twitterStreamManager.disconnect();

        twitterStreamManager.reconnect();

        verify(client).connect();
        verify(client).stop();
        verify(client, never()).reconnect();
        verify(appender).doAppend(argThat(new LoggingArgumentMatcher("Connected to the data stream.")));
        verify(appender).doAppend(argThat(new LoggingArgumentMatcher("Permanently disconnected from the data stream.")));
        verify(appender).doAppend(argThat(new LoggingArgumentMatcher("Data stream has been permanently disconnected. Unable to reconnect.")));
    }

    @Test
    public void disconnectClient() {
        twitterStreamManager.connect();

        twitterStreamManager.disconnect();

        verify(client).connect();
        verify(client).stop();
        verify(appender).doAppend(argThat(new LoggingArgumentMatcher("Connected to the data stream.")));
        verify(appender).doAppend(argThat(new LoggingArgumentMatcher("Permanently disconnected from the data stream.")));
    }

    @Test
    public void disconnectClientFailure() throws Exception {
        twitterStreamManager.connect();
        doThrow(new RuntimeException()).when(client).stop();

        twitterStreamManager.disconnect();

        verify(client).connect();
        verify(client).stop();
        verify(appender).doAppend(argThat(new LoggingArgumentMatcher("Connected to the data stream.")));
        verify(appender).doAppend(argThat(new LoggingArgumentMatcher("Failed to disconnect from the data stream.")));
    }

    @Test
    public void disconnectUnconnectedClient() {
        twitterStreamManager.disconnect();

        verify(client, never()).stop();
        verify(appender).doAppend(argThat(new LoggingArgumentMatcher("Not connected to the data stream. Unable to disconnect.")));
    }

    @Test
    public void disconnectDisconnectedClient() {
        twitterStreamManager.connect();
        twitterStreamManager.disconnect();

        twitterStreamManager.disconnect();

        verify(client).connect();
        verify(client).stop();
        verify(appender).doAppend(argThat(new LoggingArgumentMatcher("Connected to the data stream.")));
        verify(appender).doAppend(argThat(new LoggingArgumentMatcher("Permanently disconnected from the data stream.")));
        verify(appender).doAppend(argThat(new LoggingArgumentMatcher("Data stream has been permanently disconnected. Unable to disconnect.")));
    }
}
