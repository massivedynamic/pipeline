package massivedynamic.pipeline.feed.twitter.hbc;

import com.typesafe.config.Config;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.concurrent.BlockingQueue;

import static org.junit.Assert.assertNotNull;

/**
 * @author lmedina
 */
@RunWith(MockitoJUnitRunner.class)
public class ClientFactoryTest {
    @Mock
    private Config configuration;

    @Mock
    private BlockingQueue<String> messageQueue;

    @Test
    public void createValidClientFactory() {
        assertNotNull("ClientFactory must not be null.", new ClientFactory(configuration, messageQueue));
    }

    @Test(expected = IllegalArgumentException.class)
    public void createClientFactoryWithNullConfiguration() {
        new ClientFactory(null, messageQueue);
    }

    @Test(expected = IllegalArgumentException.class)
    public void createClientFactoryWithNullMessageQueue() {
        new ClientFactory(configuration, null);
    }
}
