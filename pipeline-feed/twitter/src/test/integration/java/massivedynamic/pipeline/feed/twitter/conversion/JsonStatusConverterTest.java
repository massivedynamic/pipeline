package massivedynamic.pipeline.feed.twitter.conversion;

import massivedynamic.pipeline.core.data.conversion.Converter;
import massivedynamic.pipeline.test.io.ResourceFileLoader;
import org.joda.time.DateTime;
import org.junit.Test;
import twitter4j.Status;

import java.text.ParseException;

import static org.junit.Assert.*;

/**
 * @author lmedina
 */
public class JsonStatusConverterTest {
    private Converter<String, Status> jsonStatusConverter = new JsonStatusConverter();

    private ResourceFileLoader resourceFileLoader = new ResourceFileLoader();

    @Test
    public void convertJson() throws ParseException {
        String json = resourceFileLoader.loadString(StatusMessageConverterTest.class, "tweets/sample-tweet.json");
        Status status = jsonStatusConverter.convert(json);

        assertNotNull("status must not be null.", status);
        assertEquals("Actual createdAt did not match expected value.", new DateTime(status.getCreatedAt()).getMillis(), status.getCreatedAt().getTime());
        assertEquals("Actual id did not match expected value.", "441101316002447360", Long.toString(status.getId()));
        assertEquals("Actual text did not match expected value.", "Y es que a primera gimnasia pegarme un tiro.", status.getText());
        assertNull("source must be null.", status.getSource());
        assertFalse("isTruncated must be false.", status.isTruncated());
        assertEquals("Actual inReplyToStatusId did not match expected value.", -1, status.getInReplyToStatusId());
        assertEquals("Actual inReplyToUserId did not match expected value.", -1, status.getInReplyToUserId());
        assertFalse("isFavorited must be false.", status.isFavorited());
        assertFalse("isRetweeted must be false.", status.isRetweeted());
        assertEquals("Actual favoriteCount did not match expected value.", 0, status.getFavoriteCount());
        assertNull("inReplyToScreenName must be null.", status.getInReplyToScreenName());
        assertNull("geoLocation must be null.", status.getGeoLocation());
        assertNull("place must be null.", status.getPlace());
        assertEquals("Actual retweetCount did not match expected value.", 0, status.getRetweetCount());
        assertFalse("isPossiblySensitive must be false.", status.isPossiblySensitive());
        assertEquals("Actual lang did not match expected value.", "es", status.getLang());
        assertEquals("Actual contributorsIDs did not match expected value.", 0, status.getContributors().length);
        assertNull("retweetedStatus must be null.", status.getRetweetedStatus());
        assertEquals("Actual userMentionEntities did not match expected value.", 0, status.getUserMentionEntities().length);
        assertEquals("Actual urlEntities did not match expected value.", 0, status.getURLEntities().length);
        assertEquals("Actual hashtagEntities did not match expected value.", 0, status.getHashtagEntities().length);
        assertEquals("Actual mediaEntities did not match expected value.", 0, status.getMediaEntities().length);
        assertEquals("Actual symbolEntities did not match expected value.", 0, status.getSymbolEntities().length);
        assertEquals("Actual currentUserRetweetId did not match expected value.", -1, status.getCurrentUserRetweetId());
        assertNull("scopes must be null.", status.getScopes());
        assertEquals("Actual user id did not match expected value.", 602796730, status.getUser().getId());
        assertEquals("Actual user name did not match expected value.", "Gema_Panadero", status.getUser().getName());
        assertEquals("Actual user screenName did not match expected value.", "Gema_Panadero", status.getUser().getScreenName());
        assertEquals("Actual user location did not match expected value.", "Albacete.", status.getUser().getLocation());
        assertEquals("Actual user description did not match expected value.", "Callando bocas desde 1999. No se puede ser fuerte con alguien que es tu debilidad. || Four Princess || @Andrea_Blesa || @CdeCandela.", status.getUser().getDescription());
        assertEquals("Actual number of user descriptionURLEntities did not match expected value.", 0, status.getUser().getDescriptionURLEntities().length);
        assertNotNull("User urlEntity must not be null.", status.getUser().getURLEntity());
        assertFalse("User isContributorsEnabled must be false.", status.getUser().isContributorsEnabled());
        assertEquals("Actual user profileImageUrl did not match expected value.", "http://pbs.twimg.com/profile_images/440473442878763008/_SWh9aZK_normal.jpeg", status.getUser().getProfileImageURL());
        assertEquals("Actual user profileImageUrlHttps did not match expected value.", "https://pbs.twimg.com/profile_images/440473442878763008/_SWh9aZK_normal.jpeg", status.getUser().getProfileImageURLHttps());
        assertNull("User url must be null.", status.getUser().getURL());
        assertFalse("User isProtected must be false.", status.getUser().isProtected());
        assertEquals("Actual user followersCount did not match expected value.", 365, status.getUser().getFollowersCount());
        assertNull("User status must be null.", status.getUser().getStatus());
        assertEquals("Actual user profileBackgroundColor did not match expected value.", "B57BEB", status.getUser().getProfileBackgroundColor());
        assertEquals("Actual user profileTextColor did not match expected value.", "5E412F", status.getUser().getProfileTextColor());
        assertEquals("Actual user profileLinkColor did not match expected value.", "F5A1C0", status.getUser().getProfileLinkColor());
        assertEquals("Actual user profileSidebarFillColor did not match expected value.", "78C0A8", status.getUser().getProfileSidebarFillColor());
        assertEquals("Actual user profileSidebarBorderColor did not match expected value.", "FFFFFF", status.getUser().getProfileSidebarBorderColor());
        assertTrue("User profileUseBackgroundImage must be true.", status.getUser().isProfileUseBackgroundImage());
        assertFalse("User showAllInlineMedia must be false.", status.getUser().isShowAllInlineMedia());
        assertEquals("Actual user friendsCount did not match expected value.", 365, status.getUser().getFriendsCount());
        assertEquals("Actual user createdAt did not match expected value.", new DateTime(status.getUser().getCreatedAt()).getMillis(), status.getUser().getCreatedAt().getTime());
        assertEquals("Actual user favouritesCount did not match expected value.", 391, status.getUser().getFavouritesCount());
        assertEquals("Actual user utcOffset did not match expected value.", 7200, status.getUser().getUtcOffset());
        assertEquals("Actual user timeZone did not match expected value.", "Athens", status.getUser().getTimeZone());
        assertEquals("Actual user profileBackgroundImageUrl did not match expected value.", "http://pbs.twimg.com/profile_background_images/378800000083983233/e632f4ab51e3d73e0e609b58360d54a7.png", status.getUser().getProfileBackgroundImageURL());
        assertEquals("Actual user profileBackgroundImageUrlHttps did not match expected value.", "https://pbs.twimg.com/profile_background_images/378800000083983233/e632f4ab51e3d73e0e609b58360d54a7.png", status.getUser().getProfileBackgroundImageUrlHttps());
        assertEquals("Actual user profileBannerImageUrl did not match expected value.", "https://pbs.twimg.com/profile_banners/602796730/1393851947/web", status.getUser().getProfileBannerURL());
        assertTrue("User profileBackgroundTiled must be true.", status.getUser().isProfileBackgroundTiled());
        assertEquals("Actual user lang did not match expected value.", "es", status.getUser().getLang());
        assertEquals("Actual user statusesCount did not match expected value.", 2980, status.getUser().getStatusesCount());
        assertTrue("User isGeoEnabled must be true.", status.getUser().isGeoEnabled());
        assertFalse("User isVerified must be false.", status.getUser().isVerified());
        assertFalse("User translator must be false.", status.getUser().isTranslator());
        assertEquals("Actual user listedCount did not match expected value.", 1, status.getUser().getListedCount());
        assertFalse("User isFollowRequestSent must be false.", status.getUser().isFollowRequestSent());
        assertNull("User rateLimitStatus must be null.", status.getUser().getRateLimitStatus());
        assertEquals("Actual user accessLevel did not match expected value.", 0, status.getUser().getAccessLevel());
        assertNull("rateLimitStatus must be null.", status.getRateLimitStatus());
        assertEquals("Actual accessLevel did not match expected value.", 0, status.getAccessLevel());
    }

    @Test
    public void convertInvalidJson() {
        String json = resourceFileLoader.loadString(StatusMessageConverterTest.class, "tweets/sample-tweet-invalid.json");
        Status status = jsonStatusConverter.convert(json);

        assertNull("status must be null.", status);
    }

    @Test(expected = IllegalArgumentException.class)
    public void convertNullJson() {
        jsonStatusConverter.convert(null);
    }

    @Test
    public void convertNonStatus() {
        String json = resourceFileLoader.loadString(StatusMessageConverterTest.class, "tweets/sample-tweet-deleted.json");
        Status status = jsonStatusConverter.convert(json);

        assertNull("status must be null.", status);
    }
}

