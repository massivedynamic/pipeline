package massivedynamic.pipeline.feed.twitter.hbc;

import ch.qos.logback.classic.Logger;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.Appender;
import com.typesafe.config.ConfigFactory;
import massivedynamic.pipeline.test.logging.LoggingArgumentMatcher;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.slf4j.LoggerFactory;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.*;
import static org.mockito.Matchers.argThat;
import static org.mockito.Mockito.verify;

/**
 * @author lmedina
 */
@RunWith(MockitoJUnitRunner.class)
public class TwitterStreamManagerLiveTest {
    private static final long POLLING_TIMEOUT_MILLIS = 60000L;
    private static final long WAIT_FOR_QUEUE_TO_FILL_MILLIS = 5000L;
    private static final long WAIT_BEFORE_CONNECTING_AGAIN = 3000L;

    @Mock
    private Appender<ILoggingEvent> appender;

    private BlockingQueue<String> messageQueue;
    private TwitterStreamManager twitterStreamManager;

    @Before
    public void setUp() throws InterruptedException {
        ((Logger) LoggerFactory.getLogger(Logger.ROOT_LOGGER_NAME)).addAppender(appender);

        messageQueue = new LinkedBlockingQueue<>();
        twitterStreamManager = new TwitterStreamManager(new ClientFactory(ConfigFactory.load("configs/gardenhose-application"), messageQueue).create());

        Thread.sleep(WAIT_BEFORE_CONNECTING_AGAIN);
    }

    @Test
    public void successfullyConnectToTwitter() throws InterruptedException {
        twitterStreamManager.connect();

        assertNotNull("Failed to connect to data stream.", messageQueue.poll(POLLING_TIMEOUT_MILLIS, TimeUnit.MILLISECONDS));

        twitterStreamManager.disconnect();
        verify(appender).doAppend(argThat(new LoggingArgumentMatcher("Connected to the data stream.")));
        verify(appender).doAppend(argThat(new LoggingArgumentMatcher("Permanently disconnected from the data stream.")));
    }

    @Test
    public void successfullyDisconnectFromTwitter() throws InterruptedException {
        twitterStreamManager.connect();
        messageQueue.poll(POLLING_TIMEOUT_MILLIS, TimeUnit.MILLISECONDS);

        twitterStreamManager.disconnect();
        int messageQueueSizeAfterDisconnecting = messageQueue.size();
        Thread.sleep(WAIT_FOR_QUEUE_TO_FILL_MILLIS);

        assertTrue("No messages were received from the data stream. Failed to connect.", messageQueue.size() > 0);
        assertTrue("Failed to disconnect from data stream.", messageQueueSizeAfterDisconnecting == messageQueue.size());
        verify(appender).doAppend(argThat(new LoggingArgumentMatcher("Connected to the data stream.")));
        verify(appender).doAppend(argThat(new LoggingArgumentMatcher("Permanently disconnected from the data stream.")));
    }
}
