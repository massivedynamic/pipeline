package massivedynamic.pipeline.feed.twitter.conversion;

import org.apache.commons.lang.StringUtils;
import massivedynamic.pipeline.core.data.Pipeline;
import massivedynamic.pipeline.core.data.conversion.Converter;
import massivedynamic.pipeline.test.io.ResourceFileLoader;
import org.joda.time.DateTimeUtils;
import org.junit.Before;
import org.junit.Test;
import twitter4j.Status;

import static org.junit.Assert.*;

/**
 * @author lmedina
 */
public class StatusMessageConverterTest {
    private static final long MARCH_23_2014 = 1395532800000L;

    private ResourceFileLoader resourceFileLoader = new ResourceFileLoader();

    private Converter<String, Status> jsonStatusConverter = new JsonStatusConverter();
    private Converter<Status, Pipeline.Message> statusMessageConverter = new StatusMessageConverter();

    @Before
    public void setUp() {
        DateTimeUtils.setCurrentMillisFixed(MARCH_23_2014);
    }

    @Test
    public void convertStatus() {
        String json = resourceFileLoader.loadString(StatusMessageConverterTest.class, "tweets/sample-tweet.json");
        Status status = jsonStatusConverter.convert(json);
        Pipeline.Message message = statusMessageConverter.convert(status);

        assertTrue("message must be initialized.", message.isInitialized());
        assertTrue("message header must be initialized.", message.getHeader().isInitialized());
        assertEquals("Actual acquiredTime did not match expected value.", MARCH_23_2014, message.getHeader().getAcquiredTime());
        assertTrue("message body must be initialized.", message.getBody().isInitialized());
        assertEquals("Actual id did not match expected value.", "441101316002447360", message.getBody().getId());
        assertEquals("Actual url did not match expected value.", "http:/twitter.com/Gema_Panadero/status/441101316002447360", message.getBody().getUrl());
        assertEquals("Actual text did not match expected value.", "Y es que a primera gimnasia pegarme un tiro.", message.getBody().getText());
        assertEquals("Actual author id did not match expected value.", "602796730", message.getBody().getAuthor().getId());
        assertEquals("Actual author name did not match expected value.", "Gema_Panadero", message.getBody().getAuthor().getName());
        assertEquals("Actual author userName did not match expected value.", "Gema_Panadero", message.getBody().getAuthor().getUserName());
        assertEquals("Actual author image url did not match expected value.", "http://pbs.twimg.com/profile_images/440473442878763008/_SWh9aZK_bigger.jpeg", message.getBody().getAuthor().getImageUrl());
        assertEquals("Actual author gender did not match expected value.", "", message.getBody().getAuthor().getGender());
        assertEquals("Actual author profile url did not match expected value.", "http:/twitter.com/Gema_Panadero", message.getBody().getAuthor().getProfileUrl());
        assertEquals("Actual feed did not match expected value.", Pipeline.Feed.TWITTER, message.getBody().getFeed());
        assertEquals("Actual published time did not match expected value.", 1394001723000L, message.getBody().getPublishedTime());
        assertEquals("Actual language did not match expected value.", "es", message.getBody().getLanguage());
        assertEquals("Actual source did not match expected value.", StringUtils.EMPTY, message.getBody().getSource());
        assertEquals("Actual geoLocation latitude did not match expected value.", 0.0, message.getBody().getGeoLocation().getCoordinates().getLatitude(), 0);
        assertEquals("Actual geoLocation longitude did not match expected value.", 0.0, message.getBody().getGeoLocation().getCoordinates().getLongitude(), 0);
        assertEquals("Actual geoLocation country did not match expected value.", StringUtils.EMPTY, message.getBody().getGeoLocation().getCountry());
        assertEquals("Actual geoLocation subdivision did not match expected value.", StringUtils.EMPTY, message.getBody().getGeoLocation().getSubdivision());
        assertEquals("Actual geoLocation city did not match expected value.", StringUtils.EMPTY, message.getBody().getGeoLocation().getCity());
        assertEquals("Actual number of urls did not match expected value.", 0, message.getBody().getUrlsCount());
        assertFalse("message must not be a reply.", message.getBody().getReply().getIsReply());
    }

    @Test
    public void convertStatusWithSource() {
        String json = resourceFileLoader.loadString(StatusMessageConverterTest.class, "tweets/sample-tweet-source.json");
        Status status = jsonStatusConverter.convert(json);
        Pipeline.Message message = statusMessageConverter.convert(status);

        assertEquals("Actual source did not match expected value.", "Twitter for iPhone", message.getBody().getSource());
    }

    @Test
    public void convertStatusWithNonZeroGeoLocation() {
        String json = resourceFileLoader.loadString(StatusMessageConverterTest.class, "tweets/sample-tweet-geo.json");
        Status status = jsonStatusConverter.convert(json);
        Pipeline.Message message = statusMessageConverter.convert(status);

        assertNotNull("geoLocation must not be null.", message.getBody().getGeoLocation());
        assertEquals("Actual geoLocation latitude did not match expected value.", -29.63637362, message.getBody().getGeoLocation().getCoordinates().getLatitude(), 0);
        assertEquals("Actual geoLocation longitude did not match expected value.", -52.19049702, message.getBody().getGeoLocation().getCoordinates().getLongitude(), 0);
        assertEquals("Actual geoLocation country did not match expected value.", StringUtils.EMPTY, message.getBody().getGeoLocation().getCountry());
        assertEquals("Actual geoLocation subdivision did not match expected value.", StringUtils.EMPTY, message.getBody().getGeoLocation().getSubdivision());
        assertEquals("Actual geoLocation city did not match expected value.", StringUtils.EMPTY, message.getBody().getGeoLocation().getCity());
    }

    @Test
    public void convertStatusWithLinks() {
        String json = resourceFileLoader.loadString(StatusMessageConverterTest.class, "tweets/sample-tweet-links.json");
        Status status = jsonStatusConverter.convert(json);
        Pipeline.Message message = statusMessageConverter.convert(status);

        assertEquals("Actual number of urls did not match expected number of urls.", 2, message.getBody().getUrlsCount());
        assertEquals("Actual url did not match expected value.", "http://t.co/ppIyrLNzE9", message.getBody().getUrls(0));
        assertEquals("Actual url did not match expected value.", "http://t.co/niO2x7HiIQ", message.getBody().getUrls(1));
    }

    @Test
    public void convertStatusWithReply() {
        String json = resourceFileLoader.loadString(StatusMessageConverterTest.class, "tweets/sample-tweet-reply.json");
        Status status = jsonStatusConverter.convert(json);
        Pipeline.Message message = statusMessageConverter.convert(status);

        assertNotNull("Reply must not be null.", message.getBody().getReply());
        assertTrue("isReply must not be false.", message.getBody().getReply().getIsReply());
        assertEquals("Actual original message id did not match expected value.", "442497700261806081", message.getBody().getReply().getOriginalMessageId());
    }

    @Test
    public void convertTwitterHashtags() {
        String json = resourceFileLoader.loadString(StatusMessageConverterTest.class, "tweets/sample-tweet-hashtags.json");
        Status status = jsonStatusConverter.convert(json);
        Pipeline.Message message = statusMessageConverter.convert(status);

        assertEquals("Actual number of hashtags did not match expected number of hashtags.", 3, message.getBody().getExtension(Pipeline.Twitter.fields).getHashtagsCount());
        assertEquals("Actual hashtag did not match expected value.", "iphonegames", message.getBody().getExtension(Pipeline.Twitter.fields).getHashtags(0));
        assertEquals("Actual hashtag did not match expected value.", "kca", message.getBody().getExtension(Pipeline.Twitter.fields).getHashtags(1));
        assertEquals("Actual hashtag did not match expected value.", "faceofmlb", message.getBody().getExtension(Pipeline.Twitter.fields).getHashtags(2));
    }

    @Test
    public void convertTwitterMentions() {
        String json = resourceFileLoader.loadString(StatusMessageConverterTest.class, "tweets/sample-tweet-mentions.json");
        Status status = jsonStatusConverter.convert(json);
        Pipeline.Message message = statusMessageConverter.convert(status);

        assertEquals("Actual number of mentions did not match expected number of mentions.", 2, message.getBody().getExtension(Pipeline.Twitter.fields).getMentionsCount());
        assertEquals("Actual mention did not match expected value.", "shaniaJKT48", message.getBody().getExtension(Pipeline.Twitter.fields).getMentions(0));
        assertEquals("Actual mention did not match expected value.", "melodyJKT48", message.getBody().getExtension(Pipeline.Twitter.fields).getMentions(1));
    }

    @Test
    public void convertTwitterMediaUrls() {
        String json = resourceFileLoader.loadString(StatusMessageConverterTest.class, "tweets/sample-tweet-media.json");
        Status status = jsonStatusConverter.convert(json);
        Pipeline.Message message = statusMessageConverter.convert(status);

        assertEquals("Actual number of media urls did not match expected value.", 1, message.getBody().getExtension(Pipeline.Twitter.fields).getMediaUrlsCount());
        assertEquals("Actual media url did not match expected value.", "http://pbs.twimg.com/media/BjccKGCCIAAF6fJ.jpg", message.getBody().getExtension(Pipeline.Twitter.fields).getMediaUrls(0));
    }

    @Test
    public void convertTwitterRetweet() {
        String json = resourceFileLoader.loadString(StatusMessageConverterTest.class, "tweets/sample-tweet-retweet.json");
        Status status = jsonStatusConverter.convert(json);
        Pipeline.Message message = statusMessageConverter.convert(status);

        assertNotNull("Retweet must not be null.", message.getBody().getExtension(Pipeline.Twitter.fields).getRetweet());
        assertTrue("isRetweet must not be false.", message.getBody().getExtension(Pipeline.Twitter.fields).getRetweet().getIsRetweet());
        assertEquals("Actual original tweet id did not match expected value.", "447829656054792192", message.getBody().getExtension(Pipeline.Twitter.fields).getRetweet().getOriginalTweetId());
    }

    @Test
    public void convertNonStatusObject() {
        String json = resourceFileLoader.loadString(StatusMessageConverterTest.class, "tweets/sample-tweet-deleted.json");
        Status status = jsonStatusConverter.convert(json);

        Pipeline.Message message = statusMessageConverter.convert(status);

        assertFalse("message must not be initialized.", message.isInitialized());
        assertEquals("message did not match expected value.", Pipeline.Message.getDefaultInstance(), message);
    }

    @Test
    public void convertInvalidTweet() {
        String json = resourceFileLoader.loadString(StatusMessageConverterTest.class, "tweets/sample-tweet-invalid.json");
        Status status = jsonStatusConverter.convert(json);

        Pipeline.Message message = statusMessageConverter.convert(status);

        assertFalse("message must not be initialized.", message.isInitialized());
        assertEquals("message did not match expected value.", Pipeline.Message.getDefaultInstance(), message);
    }

    @Test
    public void convertNullStatus() {
        Pipeline.Message message = statusMessageConverter.convert(null);

        assertFalse("message must not be initialized.", message.isInitialized());
        assertEquals("message did not match expected value.", Pipeline.Message.getDefaultInstance(), message);
    }
}
