package massivedynamic.pipeline.feed.twitter.hbc;

import com.twitter.hbc.core.Client;
import com.twitter.hbc.core.endpoint.StatusesFirehoseEndpoint;
import com.twitter.hbc.core.endpoint.StatusesSampleEndpoint;
import com.typesafe.config.ConfigFactory;
import org.junit.Before;
import org.junit.Test;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * @author lmedina
 */
public class ClientFactoryLiveTest {
    private BlockingQueue<String> messageQueue;

    @Before
    public void setUp() {
        messageQueue = new LinkedBlockingQueue<>();
    }

    @Test
    public void createGardenhoseConnector() {
        ClientFactory factory = new ClientFactory(ConfigFactory.load("configs/gardenhose-application"), messageQueue);

        Client client = factory.create();

        assertNotNull("client must not be null", client);
        assertEquals("Endpoint type did not match expected value.", StatusesSampleEndpoint.class, client.getEndpoint().getClass());
    }

    @Test
    public void createFirehoseConnector() {
        ClientFactory factory = new ClientFactory(ConfigFactory.load("configs/firehose-application"), messageQueue);

        Client client = factory.create();

        assertNotNull("client must not be null", client);
        assertEquals("Endpoint type did not match expected value.", StatusesFirehoseEndpoint.class, client.getEndpoint().getClass());
    }
}
