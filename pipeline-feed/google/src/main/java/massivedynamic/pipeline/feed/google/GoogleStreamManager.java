package massivedynamic.pipeline.feed.google;

import massivedynamic.pipeline.core.stream.StreamManager;
import org.apache.commons.lang.Validate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author lmedina
 */
public class GoogleStreamManager implements StreamManager {
    private static final Logger LOGGER = LoggerFactory.getLogger(GoogleStreamManager.class);

    private Client client;

    public GoogleStreamManager(Client client) {
        Validate.notNull(client, "client must not be null.");

        this.client = client;
    }

    @Override
    public void connect() {
        LOGGER.info("Connected to the data stream.");
        client.connect();
    }

    @Override
    public void reconnect() {
        client.reconnect();
    }

    @Override
    public void disconnect() {
        client.disconnect();
    }
}
