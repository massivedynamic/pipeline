package massivedynamic.pipeline.feed.google;

import massivedynamic.pipeline.core.stream.StreamManager;
import massivedynamic.pipeline.core.stream.StreamManagerBuilder;

/**
 * @author lmedina
 */
public class GoogleStreamManagerBuilder extends StreamManagerBuilder<String> {
    @Override
    public StreamManager build() {
        return new GoogleStreamManager(new ClientFactory(messageQueue).create());
    }
}
