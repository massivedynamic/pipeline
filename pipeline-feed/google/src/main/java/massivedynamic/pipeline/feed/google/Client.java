package massivedynamic.pipeline.feed.google;

import org.apache.commons.lang.Validate;

import java.util.concurrent.BlockingQueue;

/**
 * @author lmedina
 */
public class Client {
    private BlockingQueue<String> messageQueue;

    public Client(BlockingQueue<String> messageQueue) {
        Validate.notNull(messageQueue, "messageQueue must not be null.");

        this.messageQueue = messageQueue;
    }

    private long previousTime = 0L;

    public void connect() {
        while (true) {
            long currentTime = System.currentTimeMillis();
            if (currentTime - previousTime >= 250) {
                messageQueue.add("google");
                previousTime = currentTime;
            }
        }
    }

    public void reconnect() {

    }

    public void disconnect() {

    }
}
