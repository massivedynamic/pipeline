package massivedynamic.pipeline.feed.facebook;

import massivedynamic.pipeline.core.stream.StreamManager;
import org.apache.commons.lang.Validate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author lmedina
 */
public class FacebookStreamManager implements StreamManager {
    private static final Logger LOGGER = LoggerFactory.getLogger(FacebookStreamManager.class);

    private Client client;

    public FacebookStreamManager(Client client) {
        Validate.notNull(client, "client must not be null.");

        this.client = client;
    }

    @Override
    public void connect() {
        LOGGER.info("Connected to the data stream.");
        client.connect();
    }

    @Override
    public void reconnect() {
        client.reconnect();
    }

    @Override
    public void disconnect() {
        client.disconnect();
    }
}
