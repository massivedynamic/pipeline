package massivedynamic.pipeline.feed.facebook;

import massivedynamic.pipeline.core.ioc.AbstractFactory;
import org.apache.commons.lang.Validate;

import java.util.concurrent.BlockingQueue;

/**
 * @author lmedina
 */
public class ClientFactory implements AbstractFactory<Client> {
    private BlockingQueue<String> messageQueue;

    public ClientFactory(BlockingQueue<String> messageQueue) {
        Validate.notNull(messageQueue, "messageQueue must not be null.");

        this.messageQueue = messageQueue;
    }

    @Override
    public Client create() {
        return new Client(messageQueue);
    }
}
