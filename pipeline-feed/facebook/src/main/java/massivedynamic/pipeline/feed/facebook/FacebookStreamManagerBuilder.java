package massivedynamic.pipeline.feed.facebook;

import massivedynamic.pipeline.core.stream.StreamManager;
import massivedynamic.pipeline.core.stream.StreamManagerBuilder;

/**
 * @author lmedina
 */
public class FacebookStreamManagerBuilder extends StreamManagerBuilder<String> {
    @Override
    public StreamManager build() {
        return new FacebookStreamManager(new ClientFactory(messageQueue).create());
    }
}
