package massivedynamic.pipeline.actor.state;

import akka.japi.Procedure;

/**
 * @author lmedina
 */
public abstract class State implements Procedure<Object> {
    public abstract void apply(Object message);
}
