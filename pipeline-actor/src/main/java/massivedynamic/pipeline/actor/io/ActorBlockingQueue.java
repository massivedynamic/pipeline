package massivedynamic.pipeline.actor.io;

import akka.actor.ActorRef;
import org.apache.commons.lang.Validate;

import java.io.Serializable;
import java.util.Collection;
import java.util.Iterator;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;

/**
 * @author lmedina
 */
public class ActorBlockingQueue<T> implements BlockingQueue<T>, Serializable {
    private ActorRef recipient;

    public ActorBlockingQueue(ActorRef recipient) {
        Validate.notNull(recipient, "recipient must not be null.");

        this.recipient = recipient;
    }

    @Override
    public boolean add(T message) {
        return tell(message);
    }

    @Override
    public boolean offer(T message) {
        return tell(message);
    }

    @Override
    public T remove() {
        return null;
    }

    @Override
    public T poll() {
        return null;
    }

    @Override
    public T element() {
        return null;
    }

    @Override
    public T peek() {
        return null;
    }

    @Override
    public void put(T message) throws InterruptedException {
        tell(message);
    }

    @Override
    public boolean offer(T message, long timeout, TimeUnit unit) throws InterruptedException {
        return tell(message);
    }

    @Override
    public T take() throws InterruptedException {
        return null;
    }

    @Override
    public T poll(long timeout, TimeUnit unit) throws InterruptedException {
        return null;
    }

    @Override
    public int remainingCapacity() {
        return 0;
    }

    @Override
    public boolean remove(Object o) {
        return false;
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        return false;
    }

    @Override
    public boolean addAll(Collection<? extends T> c) {
        return false;
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        return false;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        return false;
    }

    @Override
    public void clear() {

    }

    @Override
    public int size() {
        return 0;
    }

    @Override
    public boolean isEmpty() {
        return false;
    }

    @Override
    public boolean contains(Object o) {
        return false;
    }

    @Override
    public Iterator<T> iterator() {
        return null;
    }

    @Override
    public Object[] toArray() {
        return new Object[0];
    }

    @Override
    public <T1> T1[] toArray(T1[] a) {
        return null;
    }

    @Override
    public int drainTo(Collection<? super T> c) {
        return 0;
    }

    @Override
    public int drainTo(Collection<? super T> c, int maxElements) {
        return 0;
    }

    private boolean tell(T message) {
        recipient.tell(message, ActorRef.noSender());
        return true;
    }
}
