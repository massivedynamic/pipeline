package massivedynamic.pipeline.actor.feed;

import akka.actor.Props;
import org.apache.commons.lang.Validate;

import java.io.Serializable;

/**
 * @author lmedina
 */
public class Stream implements Serializable {
    private String name;
    private Long id;
    private Props props;

    public Stream(String name, Long id, Props props) {
        Validate.notNull(name, "name must not be null.");
        Validate.notNull(id, "id must not be null.");
        Validate.notNull(props, "props must not be null.");

        this.name = name;
        this.id = id;
        this.props = props;
    }

    public String getName() {
        return name;
    }

    public Long getId() {
        return id;
    }

    public Props getProps() {
        return props;
    }
}
