package massivedynamic.pipeline.actor.feed;

import akka.actor.ActorRef;
import akka.actor.Cancellable;
import akka.actor.Props;
import akka.contrib.pattern.ClusterSharding;
import akka.persistence.Persistent;
import akka.persistence.UntypedProcessor;
import akka.persistence.journal.leveldb.SharedLeveldbJournal;
import akka.persistence.journal.leveldb.SharedLeveldbStore;
import massivedynamic.pipeline.actor.sharding.ShardMessage;
import massivedynamic.pipeline.actor.signals.ShardSignal;
import massivedynamic.pipeline.actor.signals.StreamSignal;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import scala.concurrent.duration.Duration;

import java.util.Hashtable;
import java.util.concurrent.TimeUnit;

/**
 * @author lmedina
 */
public class WatchDogActor extends UntypedProcessor {
    private static final Logger LOGGER = LoggerFactory.getLogger(WatchDogActor.class);

    private Hashtable<String, Stream> streams;

    public static Props props() {
        return Props.create(WatchDogActor.class);
    }

    public WatchDogActor() {
        streams = new Hashtable<>();

        useSharedLeveldbJournal();
    }

    private void useSharedLeveldbJournal() {
        ActorRef store = getContext().system().actorOf(Props.create(SharedLeveldbStore.class), "store");
        SharedLeveldbJournal.setStore(store, getContext().system());
    }

    private final Cancellable ping = context().system().scheduler().schedule(
            Duration.create(1000, TimeUnit.MILLISECONDS),
            Duration.create(1000, TimeUnit.MILLISECONDS),
            self(),
            ShardSignal.PING,
            context().dispatcher(),
            null);

    @Override
    public void onReceive(Object message) throws Exception {
        if (message instanceof Persistent) {
            onPersistent((Persistent) message);
        } else if (message instanceof ShardSignal) {
            onShardSignal((ShardSignal) message);
        }
    }

    private void onPersistent(Persistent message) {
        Object payload = message.payload();

        if (payload instanceof Stream) {
            Stream stream = (Stream) payload;
            String key = String.valueOf(stream.getId());

            if (streams.get(key) == null) {
                streams.put(key, stream);
                LOGGER.info(String.format("Received new sharded stream with TypeName(%s), EntryId(%s), ShardId(%s)", stream.getName(), stream.getId(), stream.getId() % 30));
            } else {
                deleteMessage(message.sequenceNr(), true);
            }
        }
    }

    private void onShardSignal(ShardSignal message) {
        if (message == ShardSignal.PING) {
            pingStreams();
        }
    }

    private void pingStreams() {
        initializePing();
        signalPing();
    }

    private void initializePing() {
        streams.values().forEach(stream -> getRegion(stream).tell(new ShardMessage(stream.getProps(), stream.getId()), self()));
    }

    private void signalPing() {
        streams.values().forEach(stream -> getRegion(stream).tell(new ShardMessage(StreamSignal.ACTIVE, stream.getId()), self()));
    }

    private ActorRef getRegion(Stream stream) {
        return ClusterSharding.get(context().system()).shardRegion(stream.getName());
    }

    @Override
    public void postStop() {
        ping.cancel();
    }
}
