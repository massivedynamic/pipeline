package massivedynamic.pipeline.actor.feed;

import akka.actor.ActorRef;
import akka.actor.Props;
import akka.actor.Terminated;
import akka.actor.UntypedActor;
import massivedynamic.pipeline.actor.signals.StreamSignal;
import massivedynamic.pipeline.actor.state.State;

/**
 * @author lmedina
 */
public class StreamSupervisor extends UntypedActor {
    private Props props;

    private ActorRef stream;

    public static Props props() {
        return Props.create(StreamSupervisor.class);
    }

    @Override
    public void preStart() {
        getContext().become(uninitialized);
    }

    @Override
    public void onReceive(Object message) {

    }

    private final State uninitialized = new State() {
        @Override
        public void apply(Object message) {
            if (message instanceof Props) {
                onProps((Props) message);
            }
        }

        private void onProps(Props message) {
            props = message;
            initializeStream();
            getContext().become(initialized);
        }
    };

    private void initializeStream() {
        stream = getContext().actorOf(props);
        getContext().watch(stream);
    }

    private final State initialized = new State() {
        @Override
        public void apply(Object message) {
            if (message instanceof StreamSignal) {
                onStreamSignal((StreamSignal) message);
            } else if (message instanceof Terminated) {
                onTerminated();
            }
        }

        private void onStreamSignal(StreamSignal message) {
            stream.tell(message, self());
        }

        private void onTerminated() {
            getContext().unwatch(stream);
            getContext().become(uninitialized);
        }
    };
}
