package massivedynamic.pipeline.actor.feed;

import akka.actor.Props;
import akka.actor.UntypedActor;

/**
 * @author lmedina
 */
public class ArbiterActor extends UntypedActor {
    public static Props props() {
        return Props.create(ArbiterActor.class, ArbiterActor::new);
    }

    @Override
    public void onReceive(Object o) throws Exception {

    }
}
