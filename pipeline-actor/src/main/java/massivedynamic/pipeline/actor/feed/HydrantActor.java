package massivedynamic.pipeline.actor.feed;

import akka.actor.ActorRef;
import akka.actor.Props;
import akka.actor.UntypedActor;
import akka.contrib.pattern.DistributedPubSubExtension;
import akka.contrib.pattern.DistributedPubSubMediator;
import com.typesafe.config.Config;
import massivedynamic.pipeline.actor.configuration.StreamConfiguration;
import org.apache.commons.lang.Validate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author lmedina
 */
public class HydrantActor extends UntypedActor {
    private static final Logger LOGGER = LoggerFactory.getLogger(HydrantActor.class);

    private String topic;

    private ActorRef mediator;

    public static Props props(Config configuration) {
        return Props.create(HydrantActor.class, configuration);
    }

    @Override
    public void preStart() {
        mediator = DistributedPubSubExtension.get(context().system()).mediator();
    }

    public HydrantActor(Config configuration) {
        Validate.notNull(configuration, "configuration must not be null.");

        LOGGER.info("hydrant config - {}", configuration);
        this.topic = configuration.getString(StreamConfiguration.Topic.RAW);
    }

    @Override
    public void onReceive(Object message) throws Exception {
        if (message instanceof String) {
            System.out.println(message);
        }

        mediator.tell(new DistributedPubSubMediator.Publish(topic, message), self());
    }
}
