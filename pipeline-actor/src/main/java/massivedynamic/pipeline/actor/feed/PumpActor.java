package massivedynamic.pipeline.actor.feed;

import akka.actor.ActorRef;
import akka.actor.Props;
import akka.actor.UntypedActor;
import massivedynamic.pipeline.actor.signals.StreamSignal;
import massivedynamic.pipeline.actor.state.State;
import org.apache.commons.lang.Validate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author lmedina
 */
public class PumpActor extends UntypedActor {
    private static final Logger LOGGER = LoggerFactory.getLogger(PumpActor.class);

    private ActorRef recipient;

    public static Props props(ActorRef recipient) {
        return Props.create(PumpActor.class, recipient);
    }

    public PumpActor(ActorRef recipient) {
        Validate.notNull(recipient, "recipient must not be null.");

        this.recipient = recipient;
    }

    @Override
    public void onReceive(Object message) throws Exception {
        if (message instanceof StreamSignal) {
            onStreamSignal((StreamSignal) message);
        }
    }

    private void onStreamSignal(StreamSignal message) {
        if (message == StreamSignal.ACTIVE) {
            getContext().become(active);
        } else if (message == StreamSignal.STANDBY) {
            getContext().become(standby);
        }
    }

    private final State active = new State() {
        @Override
        public void apply(Object message) {
            if (message instanceof StreamSignal) {
                onStreamSignal((StreamSignal) message);
            } else {
                recipient.tell(message, self());
            }
        }

        private void onStreamSignal(StreamSignal message) {
            if (message == StreamSignal.STANDBY) {
                getContext().become(standby);
            }
        }
    };

    private final State standby = new State() {
        @Override
        public void apply(Object message) {
            if (message instanceof StreamSignal) {
                onStreamSignal((StreamSignal) message);
            }
        }

        private void onStreamSignal(StreamSignal message) {
            if (message == StreamSignal.ACTIVE) {
                getContext().become(active);
            }
        }
    };
}
