package massivedynamic.pipeline.actor.feed;

import akka.actor.ActorRef;
import akka.actor.Props;
import akka.actor.UntypedActor;
import akka.routing.Broadcast;
import akka.routing.FromConfig;
import com.typesafe.config.Config;
import massivedynamic.pipeline.actor.io.ActorBlockingQueue;
import massivedynamic.pipeline.actor.signals.StreamSignal;
import massivedynamic.pipeline.actor.signals.ValveSignal;
import massivedynamic.pipeline.actor.state.State;
import massivedynamic.pipeline.core.stream.StreamManager;
import massivedynamic.pipeline.core.stream.StreamManagerBuilder;
import org.apache.commons.lang.Validate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static massivedynamic.pipeline.actor.ActorNames.HYDRANT_ACTOR;
import static massivedynamic.pipeline.actor.ActorNames.PUMP_ACTOR;
import static massivedynamic.pipeline.actor.ActorNames.VALVE_ACTOR;

/**
 * @author lmedina
 */
public class StreamActor<T> extends UntypedActor {
    private static final Logger LOGGER = LoggerFactory.getLogger(StreamActor.class);
    private static final String STREAM = "stream";
    private static final String DOT = ".";

    private Config configuration;
    private StreamManagerBuilder<T> streamManagerBuilder;

    private ActorRef valve;
    private ActorRef pump;
    private ActorRef hydrant;

    public static <T> Props props(String streamName, StreamManagerBuilder<T> streamManagerBuilder) {
        return Props.create(StreamActor.class, streamName, streamManagerBuilder);
    }

    public StreamActor(String streamName, StreamManagerBuilder<T> streamManagerBuilder) {
        Validate.notNull(streamName, "configuration must not be null.");
        Validate.notNull(streamManagerBuilder, "streamManagerBuilder must not be null.");

        this.streamManagerBuilder = streamManagerBuilder;

        this.configuration = context().system().settings().config().getConfig(String.format("%s%s%s", STREAM, DOT, streamName));
    }

    @Override
    public void preStart() {
        hydrant = context().actorOf(FromConfig.getInstance().props(HydrantActor.props(configuration)), HYDRANT_ACTOR);
        pump = context().actorOf(FromConfig.getInstance().props(PumpActor.props(hydrant)), PUMP_ACTOR);
        valve = context().actorOf(ValveActor.props(createStreamManager()), VALVE_ACTOR);
    }

    private StreamManager createStreamManager() {
        return streamManagerBuilder.setConfiguration(configuration)
                                   .setMessageQueue(new ActorBlockingQueue<>(pump))
                                   .build();
    }

    @Override
    public void onReceive(Object message) throws Exception {
        if (message instanceof StreamSignal) {
            onStreamSignal((StreamSignal) message);
        }
    }

    private void onStreamSignal(StreamSignal message) {
        if (message == StreamSignal.ACTIVE) {
            becomeActive();
        } else if (message == StreamSignal.STANDBY) {
            becomeStandby();
        }
    }

    private void becomeActive() {
        pump.tell(new Broadcast(StreamSignal.ACTIVE), self());
        valve.tell(ValveSignal.CONNECT, self());

        getContext().become(active);

        LOGGER.info(String.format("%s changed state to ACTIVE.", self()));
    }

    private final State active = new State() {
        @Override
        public void apply(Object message) {
            if (message instanceof StreamSignal) {
                onStreamSignal((StreamSignal) message);
            }
        }

        private void onStreamSignal(StreamSignal message) {
            if (message == StreamSignal.STANDBY) {
                becomeStandby();
            }
        }
    };

    private void becomeStandby() {
        pump.tell(new Broadcast(StreamSignal.STANDBY), self());
        valve.tell(ValveSignal.CONNECT, self());

        getContext().become(standby);

        LOGGER.info(String.format("%s changed state to STANDBY.", self()));
    }

    private final State standby = new State() {
        @Override
        public void apply(Object message) {
            if (message instanceof StreamSignal) {
                onStreamSignal((StreamSignal) message);
            }
        }

        private void onStreamSignal(StreamSignal message) {
            if (message == StreamSignal.ACTIVE) {
                becomeActive();
            }
        }
    };

    @Override
    public void postStop() {
        valve.tell(ValveSignal.DISCONNECT, self());
    }
}
