package massivedynamic.pipeline.actor.feed;

import akka.actor.Props;
import akka.actor.UntypedActor;
import massivedynamic.pipeline.actor.signals.ValveSignal;
import massivedynamic.pipeline.core.stream.StreamManager;
import org.apache.commons.lang.Validate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author lmedina
 */
public class ValveActor extends UntypedActor {
    private static final Logger LOGGER = LoggerFactory.getLogger(ValveActor.class);

    private StreamManager streamManager;

    public static Props props(StreamManager streamManager) {
        return Props.create(ValveActor.class, streamManager);
    }

    public ValveActor(StreamManager streamManager) {
        Validate.notNull(streamManager, "streamManager must not be null.");

        this.streamManager = streamManager;
    }

    @Override
    public void onReceive(Object message) throws Exception {
        if (message instanceof ValveSignal) {
            onValveSignal((ValveSignal) message);
        }
    }

    private void onValveSignal(ValveSignal message) {
        if (message == ValveSignal.CONNECT) {
            streamManager.connect();
            LOGGER.info(self() + " CONNECTED");
        } else if (message == ValveSignal.RECONNECT) {
            streamManager.reconnect();
            LOGGER.info(self() + " RECONNECTED");
        } else if (message == ValveSignal.DISCONNECT) {
            streamManager.disconnect();
            LOGGER.info(self() + " DISCONNECTED");
            context().stop(self());
        }
    }

    @Override
    public void postStop() {
        streamManager.disconnect();
    }
}
