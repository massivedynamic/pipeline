package massivedynamic.pipeline.actor.configuration;

/**
 * @author lmedina
 */
public class AkkaConfiguration {
    public static class Cluster {
        public static final String SEED_NODES = "akka.cluster.seed-nodes";
    }
}
