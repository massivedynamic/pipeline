package massivedynamic.pipeline.actor.configuration;

/**
 * @author lmedina
 */
public class StreamConfiguration {
    public static String NAME = "stream.name";

    public static class Topic {
        public static final String RAW = "topic.raw";
    }
}
