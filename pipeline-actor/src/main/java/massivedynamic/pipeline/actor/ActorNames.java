package massivedynamic.pipeline.actor;

/**
 * @author lmedina
 */
public class ActorNames {
    private ActorNames() {

    }

    public static final String STREAM_ACTOR = "StreamActor";
    public static final String VALVE_ACTOR = "ValveActor";
    public static final String PUMP_ACTOR = "PumpActor";
    public static final String HYDRANT_ACTOR = "HyrdrantActor";
}
