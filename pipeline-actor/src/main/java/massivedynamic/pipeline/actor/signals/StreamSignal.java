package massivedynamic.pipeline.actor.signals;

/**
 * @author lmedina
 */
public enum StreamSignal {
    ACTIVE,
    STANDBY
}
