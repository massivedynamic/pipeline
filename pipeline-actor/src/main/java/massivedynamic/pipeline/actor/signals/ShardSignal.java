package massivedynamic.pipeline.actor.signals;

/**
 * @author lmedina
 */
public enum ShardSignal {
    PING,
    PONG
}
