package massivedynamic.pipeline.actor.signals;

/**
 * @author lmedina
 */
public enum ValveSignal {
    CONNECT,
    RECONNECT,
    DISCONNECT
}
