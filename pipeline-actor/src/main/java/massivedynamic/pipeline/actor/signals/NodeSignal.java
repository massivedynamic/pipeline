package massivedynamic.pipeline.actor.signals;

/**
 * @author lmedina
 */
public enum NodeSignal {
    START,
    STOP,
    PAUSE
}
