package massivedynamic.pipeline.actor.conversion;

import akka.actor.Props;
import akka.actor.Status;
import akka.actor.UntypedActor;
import org.apache.commons.lang.Validate;
import massivedynamic.pipeline.core.data.conversion.Converter;

/**
 * @author lmedina
 */
public class ConverterActor<TFrom, TTo> extends UntypedActor {
    private Converter<TFrom, TTo> converter;
    private Class<TFrom> fromClazz;

    public static <TFrom, TTo> Props props(Converter<TFrom, TTo> converter, Class<TFrom> fromClazz) {
        return Props.create(ConverterActor.class, () -> new ConverterActor<>(converter, fromClazz));
    }

    public ConverterActor(Converter<TFrom, TTo> converter, Class<TFrom> fromClazz) {
        Validate.notNull(converter, "converter must not be null.");
        Validate.notNull(fromClazz, "fromClazz must not be null.");

        this.converter = converter;
        this.fromClazz = fromClazz;
    }

    @Override
    public void onReceive(Object message) throws Exception {
        converter.convert(fromClazz.cast(message));

        if (fromClazz.isInstance(message)) {

        } else {
            unhandled(message);
        }
    }

    @Override
    public void unhandled(Object message) {
        sender().tell(new Status.Failure(new UnsupportedOperationException()), self());
    }
}
