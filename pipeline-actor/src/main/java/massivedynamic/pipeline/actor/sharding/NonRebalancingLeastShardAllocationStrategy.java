package massivedynamic.pipeline.actor.sharding;

import akka.actor.ActorRef;
import akka.contrib.pattern.ShardCoordinator;
import scala.collection.immutable.HashSet;
import scala.collection.immutable.IndexedSeq;
import scala.collection.immutable.Map;
import scala.collection.immutable.Set;

/**
 * @author lmedina
 */
public class NonRebalancingLeastShardAllocationStrategy implements ShardCoordinator.ShardAllocationStrategy {
    @Override
    public ActorRef allocateShard(ActorRef actorRef, String s, Map<ActorRef, IndexedSeq<String>> actorRefIndexedSeqMap) {
        return new ShardCoordinator.LeastShardAllocationStrategy(1, 3).allocateShard(actorRef, s, actorRefIndexedSeqMap);
    }

    @Override
    public Set<String> rebalance(scala.collection.immutable.Map<ActorRef, scala.collection.immutable.IndexedSeq<String>> actorRefIndexedSeqMap, Set<String> stringSet) {
        return new HashSet<>();
    }
}
