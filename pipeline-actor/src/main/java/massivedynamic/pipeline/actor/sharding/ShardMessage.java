package massivedynamic.pipeline.actor.sharding;

import org.apache.commons.lang.Validate;

import java.io.Serializable;

/**
 * @author lmedina
 */
public class ShardMessage implements Serializable {
    private Object message;
    private Long id;

    public ShardMessage(Object message, Long id) {
        Validate.notNull(message, "message must not be null.");
        Validate.notNull(id, "id must not be null.");

        this.message = message;
        this.id = id;
    }

    Object getMessage() {
        return message;
    }

    public Long getId() {
        return id;
    }
}
