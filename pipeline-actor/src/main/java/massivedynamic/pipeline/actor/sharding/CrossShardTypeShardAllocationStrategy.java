package massivedynamic.pipeline.actor.sharding;

import akka.actor.ActorRef;
import akka.contrib.pattern.ShardCoordinator;
import org.apache.commons.lang.Validate;
import scala.collection.Iterator;
import scala.collection.immutable.HashSet;
import scala.collection.immutable.IndexedSeq;
import scala.collection.immutable.Map;
import scala.collection.immutable.Set;

import java.util.HashMap;

/**
 * @author lmedina
 */
public class CrossShardTypeShardAllocationStrategy implements ShardCoordinator.ShardAllocationStrategy {
    private HashMap<String, Long> shardCount;

    public CrossShardTypeShardAllocationStrategy(HashMap<String, Long> shardCount) {
        Validate.notNull(shardCount, "shardCount must not be null.");

        this.shardCount = shardCount;
    }

    @Override
    public ActorRef allocateShard(ActorRef actorRef, String s, Map<ActorRef, IndexedSeq<String>> actorRefIndexedSeqMap) {
        synchronized (this) {
            Iterator<ActorRef> shardRegions = actorRefIndexedSeqMap.keysIterator();

            ActorRef leastPopulatedShardRegion = new ShardCoordinator.LeastShardAllocationStrategy(1, 3).allocateShard(actorRef, s, actorRefIndexedSeqMap);
            long leastPopulatedShardCount = Long.MAX_VALUE;

            while (shardRegions.hasNext()) {
                ActorRef shardRegion = shardRegions.next();
                String address = shardRegion.path().address().toString();

                if (shardCount.get(address) == null) {
                    leastPopulatedShardCount = 0;
                    leastPopulatedShardRegion = shardRegion;
                } else {
                    long shardCount = this.shardCount.get(address);

                    if (shardCount < leastPopulatedShardCount) {
                        leastPopulatedShardCount = shardCount;
                        leastPopulatedShardRegion = shardRegion;
                    }
                }
            }

            shardCount.put(leastPopulatedShardRegion.path().address().toString(), leastPopulatedShardCount + 1);
            return leastPopulatedShardRegion;
        }
    }

    @Override
    public Set<String> rebalance(scala.collection.immutable.Map<ActorRef, scala.collection.immutable.IndexedSeq<String>> actorRefIndexedSeqMap, Set<String> stringSet) {
        return new HashSet<>();
    }
}