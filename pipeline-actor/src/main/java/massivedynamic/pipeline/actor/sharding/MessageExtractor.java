package massivedynamic.pipeline.actor.sharding;

import akka.contrib.pattern.ShardRegion;
import org.apache.commons.lang.Validate;

/**
 * @author lmedina
 */
public class MessageExtractor implements ShardRegion.MessageExtractor {
    private Integer idealNumberOfShards;

    public MessageExtractor(Integer idealNumberOfShards) {
        Validate.notNull(idealNumberOfShards, "idealNumberOfShards must not be null.");

        this.idealNumberOfShards = idealNumberOfShards;
    }

    @Override
    public String entryId(Object message) {
        if (message instanceof ShardMessage) {
            long messageId = ((ShardMessage) message).getId();
            return String.valueOf(messageId);
        } else {
            return null;
        }
    }

    @Override
    public Object entryMessage(Object message) {
        if (message instanceof ShardMessage) {
            return ((ShardMessage) message).getMessage();
        } else {
            return message;
        }
    }

    @Override
    public String shardId(Object message) {
        if (message instanceof ShardMessage) {
            long messageId = ((ShardMessage) message).getId();
            return String.valueOf(messageId % idealNumberOfShards);
        } else {
            return null;
        }
    }
}
